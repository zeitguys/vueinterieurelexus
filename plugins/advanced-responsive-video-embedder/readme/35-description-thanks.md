
### Thanks ###

*   Howard Iken of [myfloridalaw.com](https://www.myfloridalaw.com) top donor, super nice to me even if I was rude and not deserved it!
*   [Ilya Grishkov](https://www.ilyagrishkov.com) for bringing up the idea and the first code to cache thumbnail urls.
*   Anybody giving contructive feedback, testing beta versions.
*   Anybody I forgot.

### Thanks to the developers of the software used in ARVE ###

*   [Shortcode UI](https://wordpress.org/plugins/shortcode-ui/), optional Plugin, utilized by ARVE
*   [Lity Lightbox](http://sorgalla.com/lity/), used in [Pro Addon](https://nextgenthemes.com/plugins/advanced-responsive-video-embedder-pro/#purchase))
