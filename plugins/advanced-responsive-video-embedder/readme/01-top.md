=== Advanced Responsive Video Embedder ===
Contributors: nico23
Donate link: https://www.paypal.me/nico23
Tags: YouTube, Vimeo, lazyload, thumbnail, video, responsive, embeds, video-embedder, iframe, lightweight, simplicity, shortcodes
Requires at least: 4.4.0
Tested up to: 4.7.3
Stable tag: trunk
License: GPL-3.0
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Easy responsive video embeds via URLs or shortcodes. Perfect drop-in replacement for WordPress' default embeds. Best plugin for videos?
