
### [Supported Providers](https://nextgenthemes.com/plugins/advanced-responsive-video-embedder-pro/#video-host-support) ###

*   allmyvideos.net
*   Alugha
*   Archive.org
*   Break
*   Brightcove
*   CollegeHumor
*   Comedy Central
*   Dailymotion
*   Facebook
*   Funny or Die
*   Gametrailers
*   IGN
*   Kickstarter
*   LiveLeak
*   Livestream
*   kla.tv
*   Metacafe
*   Movieweb
*   MPORA
*   Myspace
*   Snotr
*   Spike
*   TED Talks
*   Twitch
*   Ustream
*   RuTube.ru
*   Veoh
*   Vevo
*   Viddler
*   vidspot.net
*   Vine
*   Vimeo
*   VK
*   Vzaar
*   Wistia
*   XTube
*   Yahoo
*   Youku
*   YouTube
*   YouTube Playlist
*   HTML5 Video (in testing)
*   [All providers with responsive iframe embed codes](https://nextgenthemes.com/plugins/advanced-responsive-video-embedder-pro/documentation/#general-iframe-embedding)
