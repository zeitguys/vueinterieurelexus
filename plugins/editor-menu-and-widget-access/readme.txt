=== Editor Menu and Widget Access ===
Contributors: GuyPrimavera
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YVPWSJB4SPN5N
Tags: plugins, wordpress, widgets, widget, appearance, menus, menu, navigation, navigation menu, nav menu, admin, editor, editors, users, wp-admin
Requires at least: 3.0.1
Tested up to: 4.6
Stable tag: 2.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allow and control the Editors' access to the Menus, Widgets, Customize, Header and Background areas of WordPress admin's Appearance menu, whilst keeping the theme selection page hidden.

== Description ==

This lightweight plugin allows users with the role **Editor** to access the **Menus** and **Widgets** areas of the Appearance menu in WordPress' admin area. This is a common task that would be useful for clients to manage themselves to give them more control over their website's content.

This plugin also gives access to the **Customize** submenu and other theme options, but these can be hidden through the **options page** if you wish.

== Installation ==

1. Upload editor-menu-widget-access to the /wp-content/plugins/ directory.
2. Activate Editor Menu and Widget Access through the 'Plugins' menu in WordPress.
3. That's it! The default settings are applied automatically, and you can hide other pages in the options page if you wish.

== Frequently Asked Questions ==

= Do I need to configure this plugin or change any settings? =

No. The default settings are applied automatically once the plugin is activated, but you can choose exactly which pages you'd like to hide through the options page (in the "Appearance" menu whilst logged in as an Admin) if you wish.

== Screenshots ==

1. The new options page to control exactly what Editors can access.
2. The Appearance menu visible to the Editor (with "customize" set to "hidden" in this example).

== Changelog ==

= 2.0 =
* Options page added.
* Ability to hide each individual page from the Appearance menu.
* Customize links removed if "customize" is hidden.
* Many functions re-written.
* Tested with MultiSite.
* Tested with many popular plugins for potential conflicts.
* Tested with WordPress 4.6 Beta.

= 1.1.1 =
* Corrected Readme.txt.

= 1.1 =
* Cleaned code.

= 1.0 =
* Stable release.

= 0.1 =
* Beta release.

== Upgrade Notice ==

= 2.0 =
Large update. Whole plugin re-written.

= 1.1.1 =
Corrected Readme.txt.

= 1.1 =
Cleaned code.

= 1.0 =
This is the first stable release.

= 0.1 =
Beta release.
