<?php
/* 
Plugin Name: Tubelab 
Plugin URI: http://tubelab.mageeklab.com 
Description: Plugin for displaying Youtube video galleries on the easy way
Author: MaGeek Lab 
Version: 1.0.3
Author URI: http://www.mageeklab.com  
*/ 


define('MGL_TUBELAB_DOMAIN', 'mgl_tubelab' );
define('MGL_TUBELAB_URL_BASE', plugin_dir_url(__FILE__) );
define('MGL_TUBELAB_DIR_BASE', plugin_dir_path( __FILE__ ) );
define('MGL_TUBELAB_URL_ASSETS', MGL_TUBELAB_URL_BASE . 'assets/' );
define('MGL_YOUTUBE_INCLUDE_BASE_PATH' , dirname( __FILE__ ) );

if( !class_exists( 'WP_Http' ) )
        include_once( ABSPATH . WPINC. '/class-http.php' ); 

//Language
function mgl_tubelab_translation() {
    load_plugin_textdomain(MGL_TUBELAB_DOMAIN, false, dirname( plugin_basename( __FILE__ ) )  . '/lang/');
}
add_action('plugins_loaded', 'mgl_tubelab_translation');

//INCLUDES

//Exceptions
require('MGL_TubelabUtiles.php');
require('exceptions/MGL_YouTubeResponseException.class.php');

//ADMIN
if( is_admin() ) {
    require('MGL_TubelabAdmin.class.php');
}

//Model
require('repository/MGL_Tubelab_Model.class.php');
require('repository/MGL_Tubelab_RepositoryManager.class.php');

require('repository/MGL_Tubelab_RepositoryBase.class.php');
require('repository/MGL_Tubelab_PlaylistRepository.class.php');
require('repository/MGL_Tubelab_VideoRepository.class.php');

//Controller
require('controller/MGL_Tubelab_BaseController.class.php');
require('controller/MGL_Tubelab_ChannelController.class.php');
require('controller/MGL_Tubelab_PlaylistController.class.php');
require('controller/MGL_Tubelab_VideoController.class.php');

//Aplication
require('MGL_Tubelab.class.php');

//Visual Composer
require('vc_extend.php');

function tubelab_layers_widget() {
	// Include Layers Widgets
	if(class_exists('Layers_Widget')) {
		require_once MGL_TUBELAB_DIR_BASE . 'layers/Layers_Tubelab_Channel_Widget.php';
		require_once MGL_TUBELAB_DIR_BASE . 'layers/Layers_Tubelab_Playlist_Widget.php';
		require_once MGL_TUBELAB_DIR_BASE . 'layers/Layers_Tubelab_Video_Widget.php';
	} else {
		require_once MGL_TUBELAB_DIR_BASE . 'widgets/Tubelab_Channel_Widget.php';
		require_once MGL_TUBELAB_DIR_BASE . 'widgets/Tubelab_Playlist_Widget.php';
		require_once MGL_TUBELAB_DIR_BASE . 'widgets/Tubelab_Video_Widget.php';
	}

	

}
add_action( 'widgets_init' , 'tubelab_layers_widget', 50 );

