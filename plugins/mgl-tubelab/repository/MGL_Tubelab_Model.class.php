<?php

/**
 * Author: MaGeek Lab
 * Date: May 8, 2015
 * Version: 1.0
 * Description: That model provides the connection with Youtube Api v3 and retrieve its content
 */
class MGL_TubelabModel {

    const YOUTUBE_SERVER_API_HOST = 'https://www.googleapis.com/youtube/v3/';
    const CACHED_QUERY_KEY_LIST_ID = 'mgl_youtube_cachedQueryKeys';


    private $apiKey = '',
            $cacheExpiration;

    public function __construct() {
        $this->apiKey = $this->getApiKey();
        $this->cacheExpiration = mgl_tubelab_option('cache', 3600);
        
    }

    private function getApiKey() {
        $apiKey = '';
        $mgl_tubelab_options = get_option('mgl_tubelab', array(), true);
        if( isset( $mgl_tubelab_options['configuration']['apiKey'] ) ){
            $apiKey = trim( $mgl_tubelab_options['configuration']['apiKey'] );
        }

        if( $apiKey === '' ) throw new Exception("ApiKey is empty");

        return $apiKey;
    }

    public function getData( $queryType, $queryData, $expiration = false ) {
        if( $expiration === false ){
            $expiration = $this->cacheExpiration;
        }

        $queryId = $this->buildQueryId( $queryType, $queryData );

        if($expiration === 0) delete_transient($queryId);

        $cachedResults = $this->getCachedResults( $queryId );
        if( $cachedResults !== false ) return $cachedResults;

        $results = $this->getDataFromYoutubeServer( $queryType, $queryData );
        $this->saveCachedResults($queryId, $results, $expiration);
        return $results;
        
    }

    public function getDataFromYoutubeServer( $queryType, $queryData ){
        try{
            $request = new WP_Http;
            $result = $request->request( 
                $this->buildRequestUrl( $queryType, $queryData ), 
                array( 
                    'method' => 'GET',
                    'headers' => array('Accept' => 'application/json' )
                ) 
            );

            if ( is_wp_error( $result ) ) {
               $error_string = $result->get_error_message();
               throw new Exception('WP Error: '.$error_string);
            }

            if( $result['response']['code'] != 200 ) {
                throw new MGL_YoutubeResponseException( $result );
            }

            return json_decode( $result['body'] );
            

        }catch( Exception $e ){
            echo $e;
            throw new Exception();
        }
    }

    public function getCachedResults( $queryId ){

        $cachedResults =  get_transient( $queryId );
        if ( $cachedResults === false ) return false;

        return unserialize( base64_decode( $cachedResults ) );
    }

    public function saveCachedResults ( $queryId, $value, $expiration) {
        $value = base64_encode( serialize( $value ) );
        set_transient( $queryId, $value, $expiration );

    }

    public function buildRequestUrl( $queryType, $queryData ){

        $queryData['key'] = $this->apiKey;
        return self::YOUTUBE_SERVER_API_HOST . $queryType .  '?'. http_build_query( $queryData );
    }

    public function buildQueryId( $queryType, $queryData ) {
        return sha1( $this->buildRequestUrl( $queryType, $queryData ) );
    }
}