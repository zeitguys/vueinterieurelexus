<?php
class MGL_Tubelab_RepositoryManager {
    const CHANNEL  = 'CHANNEL';
    const PLAYLIST = 'PLAYLIST';
    const VIDEO    = 'VIDEO';

    public function getRepository( $repository ){
        
        switch ( $repository ) {
            case self::PLAYLIST :
                    return new MGL_Tubelab_PlaylistRepository();
                break;
            case self::VIDEO :
                    return new MGL_Tubelab_VideoRepository();
                break;
            
            default:
                throw new Exception( 'Repository does not exist' );
        }

        return $repositoryInstance;
    }
}