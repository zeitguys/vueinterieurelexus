<?php
class MGL_Tubelab_VideoRepository extends MGL_Tubelab_RepositoryBase {

    public function getVideoById( $id ){

        $response = $this->model->getData( 'videos' , array(
            'part'          => 'snippet,statistics,contentDetails',
            'maxResults'    => 1,
            'id'            => $id
        ));

        if( !isset($response->items[0]) ) {
            throw new Exception('Video '.$id.' not found');
        }
        
        return $response->items[0];
    }

    public function getChannelById( $channelId ){
        $response = $this->model->getData( 'channels' , array(
            'part' => 'snippet,contentDetails',
            'id' => $channelId
        ));

        if( !isset($response->items[0]) ) {
            throw new Exception('Channel ID '.$channelId.' not found');
        }

        return $response->items[0];
    }

}