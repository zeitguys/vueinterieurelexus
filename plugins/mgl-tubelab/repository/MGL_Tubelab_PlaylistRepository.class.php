<?php
class MGL_Tubelab_PlaylistRepository extends MGL_Tubelab_RepositoryBase {

    public function getChannelIdByUsername( $username ){
        $response = $this->model->getData( 'channels' , array(
            'part'          => 'id',
            'forUsername'   => $username
        ));

        if( !isset($response->items[0]) ) {
            throw new Exception('User '.$username.' not found');
        }

        return $response->items[0]->id;
    }

    public function getChannelInfoById( $channelId ){
        $response = $this->model->getData( 'channels' , array(
            'part' => 'snippet,contentDetails,brandingSettings,statistics',
            'id' => $channelId
        ));

        if( !isset($response->items[0]) ) {
            throw new Exception('Channel ID '.$channelId.' not found');
        }

        return $response->items[0];
    }

    public function getPLaylistsByChannelId( $channelId, $givenPlaylists = false ){
        

        if($givenPlaylists && $givenPlaylists != 'all') {
            $parameters = array(
                'part'      => 'snippet',
                'id'        => $givenPlaylists,
                'maxResults'=> 50
            );
        } else {
            $parameters = array(
                'part'      => 'snippet',
                'channelId' => $channelId,
                'maxResults'=> 50
            );
        }

        $response = $this->model->getData( 'playlists' , $parameters);

        return $response->items;
    }

    public function getResponseByPlayListId( $playlistId, $maxResults = 10, $pageToken ) {
        
        $parameters = array(
            'part' => 'snippet',
            'maxResults' => $maxResults,
            'playlistId' => $playlistId
        );

        if($pageToken != '') {
            $parameters['pageToken'] = $pageToken;
        }

        $response = $this->model->getData( 'playlistItems' , $parameters);

        return $response;
    }

    public function getPlayListItemsIdsByPlayListId( $response ) {
        $videoIds = array();
        foreach ($response->items as $key => $item) {
            $videoIds[] = $item->snippet->resourceId->videoId;
        }

        return $videoIds;
    }

    public function getVideosByIdArray( $videoIds ){
        $response = $this->model->getData( 'videos' , array(
            'part' => 'snippet,statistics,contentDetails',
            'maxResults' => count( $videoIds ),
            'id' => implode( ',' , $videoIds )
        ));

        return $response->items;
    }

    public function getPlayListVideosByPlayListId( $playlistId, $maxResults = 10 ) {
        $videoIds = $this->getPlayListItemsIdsByPlayListId( $playlistId, $maxResults );
        if( count($videoIds) == 0) return array();
        return $this->getVideosByIdArray( $videoIds );
    }
}