<h2><?php _e('Shortcodes', MGL_TUBELAB_DOMAIN); ?></h2>
<p><?php _e("Please keep in mind than this is just a little summary, check the full documentation in the folder with the same name from the zip you've downloaded", MGL_TUBELAB_DOMAIN); ?></p>
<div class="mgl_col_left">
    <div class="mgl_tubelab_box">
        <h3><?php _e('Playlist', MGL_TUBELAB_DOMAIN); ?></h3>
        <div class="mgl_tubelab_box_inside">
            <p><?php _e('To show a YouTube playlist', MGL_TUBELAB_DOMAIN); ?>:</p>
            [mgl_tubelab_playlist type="playlist" value="XXXX"]
            <p><em><?php _e("Here the value has to be the playlist ID, you can check for it in the YouTube url", MGL_TUBELAB_DOMAIN); ?></em></p>
            <p><?php _e("If you want to display the videos of a user / channel", MGL_TUBELAB_DOMAIN); ?></p>
            [mgl_tubelab_playlist type="channel" value="uploads" user="" channel_id=""]
            <p><em><?php _e("You only need the user or ID, you don't need both! If you change the value to likes or favorites the playlist will display this channel / user likes, favorites playlist", MGL_TUBELAB_DOMAIN); ?></em></p>
        </div>
    </div>

    <div class="mgl_tubelab_box">
        <h3><?php _e('Channel', MGL_TUBELAB_DOMAIN); ?></h3>
        <div class="mgl_tubelab_box_inside">
            <p><?php _e('To show a full YouTube channel', MGL_TUBELAB_DOMAIN); ?>:</p>
            [mgl_tubelab_channel user="XXXX" channel_id="XXXX"]
            <p><em><?php _e("To display the channel you only need the user or ID, you don't need both!", MGL_TUBELAB_DOMAIN); ?></em></p>
            <p><?php _e("By default the channel shows all the available playlist, to only show a group of them use the playlists attribute:", MGL_TUBELAB_DOMAIN); ?></p>
            [mgl_tubelab_channel user="XXXX" channel_id="XXXX" playlists="uploads,favorites"]
            <p><em><?php _e("The channel default playlists are: uploads, favorites and likes, you can also add the id of a custom playlist", MGL_TUBELAB_DOMAIN); ?></em></p>
        </div>
    </div>

    <div class="mgl_tubelab_box">
        <h3><?php _e('Video', MGL_TUBELAB_DOMAIN); ?></h3>
        <div class="mgl_tubelab_box_inside">
            <p><?php _e('To show a single video', MGL_TUBELAB_DOMAIN); ?>:</p>
            [mgl_tubelab_video video_id="XXXX"]
            <p><em><?php _e("You can get the Video ID from the YouTube url", MGL_TUBELAB_DOMAIN); ?></em></p>
        </div>
    </div>

</div>
<div class="mgl_col_right">
    <div class="mgl_tubelab_box">
        <h3><?php _e('Parameters', MGL_TUBELAB_DOMAIN); ?></h3>
        <table class="mgl_tubelab_table">
            <tr>
                <td><strong>cols</strong></td>
                <td><strong>From 1 to 12</strong></td>
            </tr>
            <tr>
                <td><strong>count</strong></td>
                <td><strong>From 1 to 50</strong></td>
            </tr>
            <tr>
                <td><strong>pagination</strong></td>
                <td><strong>true / false</strong></td>
            </tr>
            <tr>
                <td><strong>mode</strong></td>
                <td><strong>embed / lightbox / direct</strong></td>
            </tr>
            <tr>
                <td><strong>size</strong></td>
                <td><strong>default / medium / high / standard</strong></td>
            </tr>
            <tr>
                <td><strong>template</strong></td>
                <td><strong>Check below</strong></td>
            </tr>
            <tr>
                <td><strong>display</strong></td>
                <td><strong>Parts to show sepparated by commas</strong></td>
            </tr>
        </table>
    </div>
    <div class="mgl_tubelab_box">
        <h3><?php _e('Available templates', MGL_TUBELAB_DOMAIN); ?></h3>
        <table class="mgl_tubelab_table">
            <?php $templates = mgl_tubelab_templates();
            foreach ($templates as $template) { ?>
                <tr>
                    <td><strong><?php echo $template; ?></strong></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>