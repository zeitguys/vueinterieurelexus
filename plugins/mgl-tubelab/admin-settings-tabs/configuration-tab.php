<h2><?php _e('Configuration', MGL_TUBELAB_DOMAIN); ?></h2>
<div class="mgl_col_left">
    <form name="mgl_tubelab_form" method="post" action="<?php echo site_url().'/wp-admin/options-general.php?page=mgl-tubelab&tab=configuration'; ?>"> 
        <table class="form-table">
            <tr> 
                <th scope="row"><?php _e('YouTube Api Key', MGL_TUBELAB_DOMAIN); ?></th>
                <td>
                    <input class="regular-text code" type="text" name="mgl_tubelab[configuration][apiKey]" value="<?php echo $this->mglTubelabSettings['configuration']['apiKey']; ?>"  />
                    <p class="description">
                        <?php printf( __( 'In order for Tubelab to work you need to create a project and provide a valid API Key with a Google Account, you can get one from the %1$sGoogle API Console%2$s.', MGL_TUBELAB_DOMAIN ), '<a href="https://console.developers.google.com/project" target="_blank">', '</a>' ); ?>
                    </p>
                </td>
            </tr>
            <tr> 
                <th scope="row"><?php _e('Load jQuery', MGL_TUBELAB_DOMAIN); ?></th>
                <td>
                    <?php $loadJquery = ( isset( $this->mglTubelabSettings['configuration']['jquery'] ) ) ? $this->mglTubelabSettings['configuration']['jquery'] : 0; ?>
                    <label for="mgl_tubelab_configuration_jquery">
                        <input id="mgl_tubelab_configuration_jquery" type="checkbox" name="mgl_tubelab[configuration][jquery]" value="1" <?php echo checked( $loadJquery , 1 ); ?> />
                        <?php _e('Unmark this if you are already loading jQuery', MGL_TUBELAB_DOMAIN); ?>
                    </label>
                </td>
            </tr>
        </table> 
        <p class="submit">
            <input class="button button-primary" type="submit" name="Submit" value="<?php _e('Save settings', MGL_TUBELAB_DOMAIN ); ?>" />  
        </p>
    </form> 
</div>
<div class="mgl_col_right">
    <a class="mgl_banner" href="http://codecanyon.net/user/MaGeekLab?ref=mageeklab" title="Follow us on CodeCanyon" target="_blank"><img  title="Follow us on CodeCanyon"  alt="Follow us on CodeCanyon" src="<?php echo MGL_TUBELAB_URL_BASE.'assets/images/mageeklab_banner_codecanyon.png'; ?>" alt=""></a>
</div>