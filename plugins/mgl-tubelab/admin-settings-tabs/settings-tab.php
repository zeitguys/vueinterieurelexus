<h2><?php _e('Default settings', MGL_TUBELAB_DOMAIN); ?></h2>
<div class="mgl_col_left">
    <form name="mgl_tubelab_form" method="post" action="<?php echo site_url().'/wp-admin/options-general.php?page=mgl-tubelab&tab=default-settings'; ?>"> 
        <table class="form-table">
            <tr> 
                <th scope="row"><?php _e('Columns', MGL_TUBELAB_DOMAIN); ?></th>
                <?php
                    $cols       = mgl_tubelab_cols();
                    $modes      = mgl_tubelab_modes();
                    $sizes      = mgl_tubelab_sizes();
                    $templates  = mgl_tubelab_templates();
                ?>
                <td>  
                    <select name="mgl_tubelab[settings][cols]">
                        <?php foreach ($cols as $key => $col): ?>
                            <?php
                                $checked = '';
                                if($this->mglTubelabSettings['settings']['cols'] == $key):
                                    $checked = ' selected="selected"';
                                endif;
                            ?>
                            <option<?php echo $checked; ?> value="<?php echo $key; ?>"><?php echo $col; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php _e('Count', MGL_TUBELAB_DOMAIN); ?></th>
                <td>  
                    <input class="small-text" type="text" name="mgl_tubelab[settings][count]" value="<?php echo $this->mglTubelabSettings['settings']['count']; ?>"  />
                    <p class="description">
                        <?php _e('The max value is 50', MGL_TUBELAB_DOMAIN); ?>
                    </p>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php _e('Display', MGL_TUBELAB_DOMAIN); ?></th>
                <td>  
                    <input type="text" name="mgl_tubelab[settings][display]" value="<?php echo $this->mglTubelabSettings['settings']['display']; ?>"  />
                    <p class="description">
                        <?php _e('Items you want to display sepparated with commas', MGL_TUBELAB_DOMAIN); ?>
                    </p>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php _e('Mode', MGL_TUBELAB_DOMAIN); ?></th>
                <td>  
                    <select name="mgl_tubelab[settings][mode]">
                        <?php foreach ($modes as $key => $mode): ?>
                            <?php
                                $checked = '';
                                if($this->mglTubelabSettings['settings']['mode'] == $key):
                                    $checked = ' selected="selected"';
                                endif;
                            ?>
                            <option<?php echo $checked; ?> value="<?php echo $key; ?>"><?php echo $mode; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php _e('Template', MGL_TUBELAB_DOMAIN); ?></th>
                <td>  
                    <select name="mgl_tubelab[settings][template]">
                        <?php foreach ($templates as $template): ?>
                            <?php
                                $checked = '';
                                if($this->mglTubelabSettings['settings']['template'] == $template):
                                    $checked = ' selected="selected"';
                                endif;
                            ?>
                            <option<?php echo $checked; ?> value="<?php echo $template; ?>"><?php echo $template; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <p class="description">
                        <?php printf( __('Set the name of the skin, the possible values are: %1$s. You can also set the name of a custom template', MGL_TUBELAB_DOMAIN) , implode( ', ', mgl_tubelab_templates() ) ); ?>
                    </p>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php _e('Custom templates', MGL_TUBELAB_DOMAIN); ?></th>
                <td>  
                    <input type="text" name="mgl_tubelab[settings][custom_templates]" value="<?php echo $this->mglTubelabSettings['settings']['custom_templates']; ?>"  />
                    <p class="description">
                        <?php _e("Add the names of your custom templates sepparaded by commans, don't use spaces. If you've created a folder inside your theme for the template you don't need to add the name here", MGL_TUBELAB_DOMAIN); ?>
                    </p>
                </td>
            </tr>
        </table>
        <h3 class="title"><?php _e('Cache', MGL_TUBELAB_DOMAIN); ?></h3>
        <table class="form-table">
            <tr>
                <th scope="row"><?php _e('Time', MGL_TUBELAB_DOMAIN); ?></th>
                <td>  
                    <input class="small-text" type="text" name="mgl_tubelab[settings][cache]" value="<?php echo $this->mglTubelabSettings['settings']['cache']; ?>"  />
                    <p class="description">
                        <?php _e('The cache value is the time in seconds before the plugin will query again for new results', MGL_TUBELAB_DOMAIN); ?>
                    </p>
                </td>
            </tr>
        </table> 
        <h3 class="title"><?php _e('Thumbnails', MGL_TUBELAB_DOMAIN); ?></h3>
        <table class="form-table">
            <tr> 
                <th scope="row"><?php _e('Size', MGL_TUBELAB_DOMAIN); ?></th>
                <td>  
                    <select name="mgl_tubelab[settings][size]">
                        <?php foreach ($sizes as $key => $size): ?>
                            <?php
                                $checked = '';
                                if($this->mglTubelabSettings['settings']['size'] == $key):
                                    $checked = ' selected="selected"';
                                endif;
                            ?>
                            <option<?php echo $checked; ?> value="<?php echo $key; ?>"><?php echo $size['name'].' ('.$size['width'].'x'.$size['height'].')'; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <p class="description">
                        <?php _e('The thumbnail size when using the lightbox / direct mode', MGL_TUBELAB_DOMAIN); ?>
                    </p>
                </td>
            </tr>
        </table>
        <h3 class="title"><?php _e('Embed', MGL_TUBELAB_DOMAIN); ?></h3>
        <table class="form-table">
            <tr> 
                <th scope="row"><?php _e('Embed ratio', MGL_TUBELAB_DOMAIN); ?></th>
                <td>
                    <label for="mgl_tubelab_embed_width"><?php _e('Width', MGL_TUBELAB_DOMAIN); ?></label>
                    <input class="small-text" id="mgl_tubelab_embed_width" type="number" name="mgl_tubelab[settings][embedRatio][width]" value="<?php echo $this->mglTubelabSettings['settings']['embedRatio']['width']; ?>"  />
                    <label for="mgl_tubelab_embed_height"><?php _e('Height', MGL_TUBELAB_DOMAIN); ?>
                    <input class="small-text" id="mgl_tubelab_embed_height" type="number" name="mgl_tubelab[settings][embedRatio][height]" value="<?php echo $this->mglTubelabSettings['settings']['embedRatio']['height']; ?>"  />
                    <p class="description">Set the ratio for the video embeds, don't worry about the size as they are responsive so they wil always try to fit the 100% of the width</p>
                </td>
            </tr>
        </table> 
        <p class="submit">
            <input class="button button-primary" type="submit" name="Submit" value="<?php _e('Save settings', MGL_TUBELAB_DOMAIN ); ?>" />  
        </p>
    </form> 
</div>
<div class="mgl_col_right">
    <a class="mgl_banner" href="http://codecanyon.net/user/MaGeekLab?ref=mageeklab" title="Follow us on CodeCanyon" target="_blank"><img  title="Follow us on CodeCanyon"  alt="Follow us on CodeCanyon" src="<?php echo MGL_TUBELAB_URL_BASE.'assets/images/mageeklab_banner_codecanyon.png'; ?>" alt=""></a>
</div>