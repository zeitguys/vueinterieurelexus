#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Instagram Gallery\n"
"POT-Creation-Date: 2015-06-22 16:59+0100\n"
"PO-Revision-Date: 2014-09-17 14:17+0100\n"
"Last-Translator: MaGeek Lab <hello@mageeklab.com>\n"
"Language-Team: MaGeek Lab <hello@mageeklab.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.1\n"
"X-Poedit-Basepath: ../\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"

#: MGL_Tubelab.class.php:68
msgid "MGL Tubelab error: "
msgstr ""

#: MGL_TubelabAdmin.class.php:57
msgid "Settings saved"
msgstr ""

#: MGL_TubelabAdmin.class.php:109 admin-settings-tabs/configuration-tab.php:1
msgid "Configuration"
msgstr ""

#: MGL_TubelabAdmin.class.php:110 admin-settings-tabs/settings-tab.php:1
msgid "Default settings"
msgstr ""

#: MGL_TubelabAdmin.class.php:111
msgid "Documentation"
msgstr ""

#: MGL_TubelabAdmin.class.php:126
msgid "Tubelab by MaGeek Lab"
msgstr ""

#: MGL_TubelabUtiles.php:6
msgid "Lightbox"
msgstr ""

#: MGL_TubelabUtiles.php:7 admin-settings-tabs/settings-tab.php:124
msgid "Embed"
msgstr ""

#: MGL_TubelabUtiles.php:8
msgid "Direct link"
msgstr ""

#: MGL_TubelabUtiles.php:68 admin-settings-tabs/documentation-tab.php:17
#: layers/Layers_Tubelab_Channel_Widget.php:207
msgid "Channel"
msgstr ""

#: MGL_TubelabUtiles.php:69 admin-settings-tabs/documentation-tab.php:5
#: layers/Layers_Tubelab_Playlist_Widget.php:207
msgid "Playlist"
msgstr ""

#: MGL_TubelabUtiles.php:251
msgid "Play"
msgstr ""

#: admin-settings-tabs/configuration-tab.php:6
msgid "YouTube Api Key"
msgstr ""

#: admin-settings-tabs/configuration-tab.php:10
#, php-format
msgid ""
"In order for Tubelab to work you need to create a project and provide a "
"valid API Key with a Google Account, you can get one from the %1$sGoogle API "
"Console%2$s."
msgstr ""

#: admin-settings-tabs/configuration-tab.php:15
msgid "Load jQuery"
msgstr ""

#: admin-settings-tabs/configuration-tab.php:19
msgid "Unmark this if you are already loading jQuery"
msgstr ""

#: admin-settings-tabs/configuration-tab.php:25
#: admin-settings-tabs/settings-tab.php:138
msgid "Save settings"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:1
msgid "Shortcodes"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:2
msgid ""
"Please keep in mind than this is just a little summary, check the full "
"documentation in the folder with the same name from the zip you've downloaded"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:7
msgid "To show a YouTube playlist"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:9
msgid ""
"Here the value has to be the playlist ID, you can check for it in the "
"YouTube url"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:10
msgid "If you want to display the videos of a user / channel"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:12
msgid ""
"You only need the user or ID, you don't need both! If you change the value "
"to likes or favorites the playlist will display this channel / user likes, "
"favorites playlist"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:19
msgid "To show a full YouTube channel"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:21
msgid ""
"To display the channel you only need the user or ID, you don't need both!"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:22
msgid ""
"By default the channel shows all the available playlist, to only show a "
"group of them use the playlists attribute:"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:24
msgid ""
"The channel default playlists are: uploads, favorites and likes, you can "
"also add the id of a custom playlist"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:29
#: layers/Layers_Tubelab_Video_Widget.php:173
msgid "Video"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:31
msgid "To show a single video"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:33
msgid "You can get the Video ID from the YouTube url"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:40
msgid "Parameters"
msgstr ""

#: admin-settings-tabs/documentation-tab.php:73
msgid "Available templates"
msgstr ""

#: admin-settings-tabs/settings-tab.php:6
#: layers/Layers_Tubelab_Channel_Widget.php:150
#: layers/Layers_Tubelab_Playlist_Widget.php:150 vc_extend.php:36
#: widgets/Tubelab_Channel_Widget.php:86
#: widgets/Tubelab_Playlist_Widget.php:96
msgid "Columns"
msgstr ""

#: admin-settings-tabs/settings-tab.php:28
#: layers/Layers_Tubelab_Channel_Widget.php:143
#: layers/Layers_Tubelab_Playlist_Widget.php:143 vc_extend.php:29
#: widgets/Tubelab_Channel_Widget.php:82
#: widgets/Tubelab_Playlist_Widget.php:92
msgid "Count"
msgstr ""

#: admin-settings-tabs/settings-tab.php:32
msgid "The max value is 50"
msgstr ""

#: admin-settings-tabs/settings-tab.php:37
#: layers/Layers_Tubelab_Channel_Widget.php:157
#: layers/Layers_Tubelab_Playlist_Widget.php:157
#: layers/Layers_Tubelab_Video_Widget.php:134 vc_extend.php:46
#: vc_extend.php:180 widgets/Tubelab_Channel_Widget.php:133
#: widgets/Tubelab_Playlist_Widget.php:143 widgets/Tubelab_Video_Widget.php:89
msgid "Display"
msgstr ""

#: admin-settings-tabs/settings-tab.php:41
msgid "Items you want to display sepparated with commas"
msgstr ""

#: admin-settings-tabs/settings-tab.php:46
#: layers/Layers_Tubelab_Channel_Widget.php:253
#: layers/Layers_Tubelab_Channel_Widget.php:260
#: layers/Layers_Tubelab_Playlist_Widget.php:266
#: layers/Layers_Tubelab_Playlist_Widget.php:273
#: layers/Layers_Tubelab_Video_Widget.php:194
#: layers/Layers_Tubelab_Video_Widget.php:201 vc_extend.php:54
#: vc_extend.php:161 widgets/Tubelab_Channel_Widget.php:97
#: widgets/Tubelab_Playlist_Widget.php:107 widgets/Tubelab_Video_Widget.php:67
msgid "Mode"
msgstr ""

#: admin-settings-tabs/settings-tab.php:62
#: layers/Layers_Tubelab_Channel_Widget.php:267
#: layers/Layers_Tubelab_Playlist_Widget.php:280
#: layers/Layers_Tubelab_Video_Widget.php:208 vc_extend.php:62
#: vc_extend.php:169 widgets/Tubelab_Channel_Widget.php:108
#: widgets/Tubelab_Playlist_Widget.php:118 widgets/Tubelab_Video_Widget.php:78
msgid "Template"
msgstr ""

#: admin-settings-tabs/settings-tab.php:76
#: layers/Layers_Tubelab_Channel_Widget.php:278
#: layers/Layers_Tubelab_Playlist_Widget.php:291
#, php-format
msgid ""
"Set the name of the skin, the possible values are: %1$s. You can also set "
"the name of a custom template"
msgstr ""

#: admin-settings-tabs/settings-tab.php:81
msgid "Custom templates"
msgstr ""

#: admin-settings-tabs/settings-tab.php:85
msgid ""
"Add the names of your custom templates sepparaded by commans, don't use "
"spaces. If you've created a folder inside your theme for the template you "
"don't need to add the name here"
msgstr ""

#: admin-settings-tabs/settings-tab.php:90
msgid "Cache"
msgstr ""

#: admin-settings-tabs/settings-tab.php:93
msgid "Time"
msgstr ""

#: admin-settings-tabs/settings-tab.php:97
msgid ""
"The cache value is the time in seconds before the plugin will query again "
"for new results"
msgstr ""

#: admin-settings-tabs/settings-tab.php:102
msgid "Thumbnails"
msgstr ""

#: admin-settings-tabs/settings-tab.php:105
msgid "Size"
msgstr ""

#: admin-settings-tabs/settings-tab.php:119
msgid "The thumbnail size when using the lightbox / direct mode"
msgstr ""

#: admin-settings-tabs/settings-tab.php:127
msgid "Embed ratio"
msgstr ""

#: admin-settings-tabs/settings-tab.php:129
msgid "Width"
msgstr ""

#: admin-settings-tabs/settings-tab.php:131
msgid "Height"
msgstr ""

#: controller/MGL_Tubelab_ChannelController.class.php:79
msgid "Likes"
msgstr ""

#: controller/MGL_Tubelab_ChannelController.class.php:82
msgid "Favorites"
msgstr ""

#: controller/MGL_Tubelab_ChannelController.class.php:85
msgid "Videos"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:19 vc_extend.php:92
#: widgets/Tubelab_Channel_Widget.php:14 widgets/Tubelab_Channel_Widget.php:52
msgid "Tubelab Channel"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:26
msgid "Show a YouTube channel with Tubelab"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:134
#: layers/Layers_Tubelab_Playlist_Widget.php:134
msgid "Columns & count"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:164
#: layers/Layers_Tubelab_Playlist_Widget.php:164
#: layers/Layers_Tubelab_Video_Widget.php:141
msgid "Item display"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:171
#: layers/Layers_Tubelab_Playlist_Widget.php:171
#: layers/Layers_Tubelab_Video_Widget.php:148 vc_extend.php:71
#: vc_extend.php:188 widgets/Tubelab_Channel_Widget.php:137
#: widgets/Tubelab_Playlist_Widget.php:147 widgets/Tubelab_Video_Widget.php:93
msgid "Thumbnail size"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:179
#: layers/Layers_Tubelab_Playlist_Widget.php:179
msgid "Show Pagination"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:215
#: layers/Layers_Tubelab_Playlist_Widget.php:242
msgid "YouTube username"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:227
#: layers/Layers_Tubelab_Playlist_Widget.php:254
msgid "YouTube Channel ID"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:239 vc_extend.php:98
#: widgets/Tubelab_Channel_Widget.php:78
msgid "Playlists"
msgstr ""

#: layers/Layers_Tubelab_Channel_Widget.php:250
msgid ""
"By default the channel will load all the available playlists, if you want "
"you can specify the Ids sepparated by commas, use \"uploads\" or \"likes\" "
"for the uploads playlist and the likes playlist"
msgstr ""

#: layers/Layers_Tubelab_Playlist_Widget.php:19 vc_extend.php:106
#: widgets/Tubelab_Playlist_Widget.php:14
#: widgets/Tubelab_Playlist_Widget.php:50
msgid "Tubelab Videos"
msgstr ""

#: layers/Layers_Tubelab_Playlist_Widget.php:26
msgid "Show a YouTube playlist with Tubelab"
msgstr ""

#: layers/Layers_Tubelab_Playlist_Widget.php:214
#: layers/Layers_Tubelab_Playlist_Widget.php:221 vc_extend.php:110
msgid "Type"
msgstr ""

#: layers/Layers_Tubelab_Playlist_Widget.php:228 vc_extend.php:118
#: widgets/Tubelab_Playlist_Widget.php:80
msgid "Value"
msgstr ""

#: layers/Layers_Tubelab_Playlist_Widget.php:239
msgid ""
"In the value field you need to set the playlist ID if you want to display a "
"custom playlist or the value \"uploads\" / \"likes\" to show the uploaded/"
"liked videos of an user or channel"
msgstr ""

#: layers/Layers_Tubelab_Video_Widget.php:19
#: widgets/Tubelab_Video_Widget.php:14 widgets/Tubelab_Video_Widget.php:50
msgid "Tubelab Video"
msgstr ""

#: layers/Layers_Tubelab_Video_Widget.php:26
msgid "Show a YouTube video with Tubelab"
msgstr ""

#: layers/Layers_Tubelab_Video_Widget.php:180 vc_extend.php:155
#: widgets/Tubelab_Video_Widget.php:63
msgid "Video ID"
msgstr ""

#: layers/Layers_Tubelab_Video_Widget.php:191 vc_extend.php:157
msgid ""
"The ID of the video, you can get it from the url like https://www.youtube."
"com/watch?v=XXXXX, where XXXXX is the ID"
msgstr ""

#: layers/Layers_Tubelab_Video_Widget.php:219
msgid ""
"If your custom template is not available in the dropdown, add it in the "
"settings page"
msgstr ""

#: templates/default/channel.php:16
#, php-format
msgid "%1$s videos"
msgstr ""

#: templates/default/channel.php:17
#, php-format
msgid "%1$s views"
msgstr ""

#: templates/default/channel.php:18
#, php-format
msgid "%1$s subscribers"
msgstr ""

#: templates/default/channel.php:59 templates/default/container.php:7
msgid "Previous page"
msgstr ""

#: templates/default/channel.php:62 templates/default/container.php:10
msgid "Next page"
msgstr ""

#: vc_extend.php:16
msgid "Username"
msgstr ""

#: vc_extend.php:18
msgid "Username to pull videos from"
msgstr ""

#: vc_extend.php:23 widgets/Tubelab_Channel_Widget.php:74
#: widgets/Tubelab_Playlist_Widget.php:88
msgid "Channel ID"
msgstr ""

#: vc_extend.php:25
msgid "ID of the channel to pull videos from"
msgstr ""

#: vc_extend.php:32
msgid "Number of videos to display"
msgstr ""

#: vc_extend.php:40
msgid "Number of columns of the gallery"
msgstr ""

#: vc_extend.php:48 vc_extend.php:182
msgid ""
"Parts of the video you want to display, set none to only show the video "
"itself"
msgstr ""

#: vc_extend.php:50 vc_extend.php:67 vc_extend.php:75 vc_extend.php:174
#: vc_extend.php:184 vc_extend.php:193
msgid "Style"
msgstr ""

#: vc_extend.php:58 vc_extend.php:165
msgid "Choose how the videos are played"
msgstr ""

#: vc_extend.php:66 vc_extend.php:173
msgid "Choose the layout you most like to display your videos"
msgstr ""

#: vc_extend.php:74 vc_extend.php:192
msgid "The size of the thumbnail size"
msgstr ""

#: vc_extend.php:80 widgets/Tubelab_Channel_Widget.php:119
#: widgets/Tubelab_Playlist_Widget.php:129
msgid "Pagination"
msgstr ""

#: vc_extend.php:83 widgets/Tubelab_Channel_Widget.php:123
#: widgets/Tubelab_Playlist_Widget.php:133
msgid "Yes"
msgstr ""

#: vc_extend.php:84 widgets/Tubelab_Channel_Widget.php:124
#: widgets/Tubelab_Playlist_Widget.php:134
msgid "No"
msgstr ""

#: vc_extend.php:86
msgid "Advanced"
msgstr ""

#: vc_extend.php:100
msgid ""
"If you don't want to display all the playlists of the channel set it's ID "
"like uploads or likes"
msgstr ""

#: vc_extend.php:113
msgid "Where do you want to display the videos from"
msgstr ""

#: vc_extend.php:120
msgid "The ID from the playlist, if it's from an user use uploads or likes"
msgstr ""

#: vc_extend.php:130 vc_extend.php:144 widgets/Tubelab_Channel_Widget.php:15
#: widgets/Tubelab_Playlist_Widget.php:15 widgets/Tubelab_Video_Widget.php:15
msgid "By MaGeek Lab"
msgstr ""

#: vc_extend.php:135 vc_extend.php:149
msgid "Social"
msgstr ""

#: vc_extend.php:143
msgid "Tubelab Single Video"
msgstr ""

#: widgets/Tubelab_Channel_Widget.php:66
#: widgets/Tubelab_Playlist_Widget.php:65
#: widgets/Tubelab_Playlist_Widget.php:69 widgets/Tubelab_Video_Widget.php:59
msgid "Title"
msgstr ""

#: widgets/Tubelab_Channel_Widget.php:70
#: widgets/Tubelab_Playlist_Widget.php:84
msgid "User"
msgstr ""
