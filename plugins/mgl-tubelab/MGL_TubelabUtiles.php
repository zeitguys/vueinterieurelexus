<?php

function mgl_tubelab_modes( $inverse = false ) {
	
	$modes = array(
      'lightbox'    => __('Lightbox', MGL_TUBELAB_DOMAIN), 
      'embed'       => __('Embed', MGL_TUBELAB_DOMAIN), 
      'direct' 		=> __('Direct link', MGL_TUBELAB_DOMAIN)
    );

	if($inverse) {
		$modes = mgl_tubelab_reverse_array($modes);
	}

	return $modes;
	
}

function mgl_tubelab_templates( $withKey = false ) {
	
	$defaultTemplates = array(
      'default',
      'dark',
      'border',
      'circle',
      'plain'
    );

	$themeTemplates = $customTemplates = array();
	$themeTemplatesDirectory = get_template_directory().'/tubelab'; 
	$tubelabOptions = get_option('mgl_tubelab');

	if( isset( $tubelabOptions['settings']['custom_templates'] ) ) {
		$customTemplates = explode(',', $tubelabOptions['settings']['custom_templates']);
	}

	if( file_exists($themeTemplatesDirectory) ) {
		$weeds = array('.', '..', '.DS_Store'); 
		$themeTemplates = array_diff(scandir($themeTemplatesDirectory), $weeds); 
	}

    $templates = array_merge($defaultTemplates, $themeTemplates, $customTemplates);

    $templates = array_map('trim', $templates);
    $templates = array_unique($templates);
	
	foreach ($templates as $key => $template) {
    	if($template == '') {
    		unset($templates[$key]);
    	}
    }
    
    if($withKey) {
    	$dummyArray = array();

    	foreach ($templates as $template) {
    		$dummyArray[$template] = $template;
    	}
    	return $dummyArray;
    }

    return $templates;
}

function mgl_tubelab_playlist_types( $inverse = false ) {
	
	$types = array(
      'channel'		=> __('Channel', MGL_TUBELAB_DOMAIN),
      'playlist'	=> __('Playlist', MGL_TUBELAB_DOMAIN)
    );

    if($inverse) {
		$types = mgl_tubelab_reverse_array($types);
	}

    return $types;
}

function mgl_tubelab_reverse_array( $givenArray ) {
	$dummyArray = array();

	foreach ($givenArray as $key => $givenValue) {
		$dummyArray[$givenValue] = $key;
	}

	return $dummyArray;
}

function mgl_tubelab_sizes( $onlyNames = false, $inverse = false ) {
	$sizes = array(
        'default'   => array( 'name' =>'Default', 'width' => 120, 'height' => 90 ),
        'medium'    => array( 'name' =>'Medium', 'width' => 320, 'height' => 180 ),
        'high'      => array( 'name' =>'High', 'width' => 480, 'height' => 360 ),
        'standard'  => array( 'name' =>'Standard', 'width' => 640, 'height' => 480 )
    );

    if($onlyNames) {
    	$dummySizes = array();

    	foreach ($sizes as $key => $size) {
    		$dummySizes[$key] = $size['name'];
    	}

    	if($inverse) {
    		$dummySizes = mgl_tubelab_reverse_array($dummySizes);
    	}

    	return $dummySizes;
    }

    return $sizes;
}

function mgl_tubelab_cols( $inverse = false ) {
    $column_options = array();

    for ($i = 1; $i <= 12; $i++) {
        $column_options[$i] = sprintf( _n( '%d column', '%d columns', $i, MGL_TUBELAB_DOMAIN ), $i );
    }

    if($inverse) {
		$column_options = mgl_tubelab_reverse_array($column_options);
	}

    return $column_options;
}

function mgl_tubelab_print_select($values, $selectedValue, $selectId, $selectName, $withKey = true) {
?>
	<select class="widefat" id="<?php echo $selectId; ?>" name="<?php echo $selectName; ?>">
		<?php 
		if($withKey) {
			foreach ($values as $key => $value) { 
				$value = ( is_array($value) ) ? $value['name'] : $value;
				$selected = ( $key == $selectedValue ) ? ' selected="selected"' : '';
				echo '<option'.$selected.' value="'.$key.'">'.$value.'</option>';
			}
		} else {
			foreach ($values as $value) { 
				$selected = ( $value == $selectedValue ) ? ' selected="selected"' : '';
				echo '<option'.$selected.' value="'.$value.'">'.$value.'</option>';
			}
		}
		?>
	</select>
<?php
}

function mgl_tubelab_option($key, $default = false) {
	$tubelabOptions = get_option('mgl_tubelab');
	if( !isset($tubelabOptions['settings'][$key]) || $tubelabOptions['settings'][$key] == '') {
		return $default;
	}
	return $tubelabOptions['settings'][$key];
}

function mgl_tubelab_cut_string($text, $size = 100) {
	$length = mb_strlen($text);
	
	if($length > $size):
		$result = mb_substr( $text , 0, $size -3).'...';
	else:
		$result = $text;
	endif;

	return $result;
}

function mgl_tubelab_get_duration($interval) {

	$durationInterval = new DateInterval($interval);

	if($durationInterval->h != 0):
		$duration = $durationInterval->format('%h:%I:%S');
	else:
		$duration = $durationInterval->format('%i:%S');
	endif;

	return $duration;
}

function mgl_tubelab_get_published($stringDate) {

	$date = new DateTime($stringDate);

	return date_i18n("d F Y",$date->getTimestamp());
}

function mgl_tubelab_display($display, $part) {
	
	$result = false;

	if(is_array($part)) {
		
		foreach ($part as $value) {
			if(mgl_tubelab_display_check($display, $value)) {
				$result = true;
			}
		}
		
	} else {

		$result = mgl_tubelab_display_check($display, $part);

	}

	return $result;
}

function mgl_tubelab_display_check($display, $value) {
	
	if(in_array($value, $display)) {
		return true;
	} else {
		return false;
	}

}

function mgl_tubelab_thumbnail($size, $thumbnails) {
	
	if(! property_exists($thumbnails, $size)) {
		$size = 'medium';
	}

	return $thumbnails->$size->url;
}

function mgl_tubelab_rich_text($input) {
	// Links
	$input = preg_replace('@(https?://([-\w.]+[-\w])+(:\d+)?(/([\w-.~:/?#\[\]\@!$&\'()*+,;=%]*)?)?)@', '<a href="$1" target="_blank">$1</a>', $input);
	return $input;
}

function mgl_tubelab_print_video($video, $mode, $size, $cssClass = '') {
	// Base CSS class
	$tubelab_video_class = 'mgl_tubelab_video';

	// Custom CSS class
	if($cssClass != ''):
		$tubelab_video_class .= ' '.$cssClass;
	endif;

	// Add class & parameters depending on mode
	$target = '';

	if($mode == 'lightbox'): $tubelab_video_class .= ' mgl_tubelab_lightbox'; endif;
	if($mode == 'direct'): $target .= ' target="_blank"'; endif;

	if($mode == 'lightbox' || $mode == 'direct'): // Print image linking to video ?>	
		<a class="<?php echo $tubelab_video_class; ?>" href="http://www.youtube.com/watch?v=<?php echo $video->id; ?>" title="<?php _e('Play', MGL_TUBELAB_DOMAIN); ?>"<?php echo $target; ?>>
			<span class="mgl_tubelab_video_play">
				<i class="mgl_tubelab-icon-play"></i>
			</span>
			<span class="mgl_tubelab_video_time">
				<?php echo mgl_tubelab_get_duration($video->contentDetails->duration); ?>
			</span>
			<img class="mgl_tubelab_video_thumbnail" src="<?php echo mgl_tubelab_thumbnail($size, $video->snippet->thumbnails); ?>" />
		</a>

	<?php else: // Print YouTube video embed 
		$embedRatio = mgl_tubelab_option('embedRatio', array( 'width' => 320, 'height' => 180 ));
	?>	
		<iframe class="mgl_tubelab_embed" width="<?php echo $embedRatio['width']; ?>" height="<?php echo $embedRatio['height']; ?>" src="https://www.youtube.com/embed/<?php echo $video->id ?>?showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
	
	<?php endif;
}