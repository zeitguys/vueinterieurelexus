<?php

/**
 * Adds Tubelab_Channel_Widget widget.
 */
class Tubelab_Channel_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'tubelab_channel_widget', // Base ID
			__( 'Tubelab Channel', MGL_TUBELAB_DOMAIN ), // Name
			array( 'description' => __( 'By MaGeek Lab', MGL_TUBELAB_DOMAIN ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

        $instance['mini'] = 'true';
		$shortcodeAttributes = '';
		foreach( $instance as $argKey => $argVal ){
            if( is_array($argVal)) { continue; }
            $shortcodeAttributes .= " $argKey" . '="'. (string)$argVal . '"';
        }

        echo do_shortcode( '[mgl_tubelab_channel ' . $shortcodeAttributes . ' ]' );
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title 			= ! empty( $instance['title'] ) ? $instance['title'] : __( 'Tubelab Channel', MGL_TUBELAB_DOMAIN );
		$user 			= ! empty( $instance['user'] ) ? $instance['user'] : '';
		$channel_id 	= ! empty( $instance['channel_id'] ) ? $instance['channel_id'] : '';
		$playlists 		= ! empty( $instance['playlists'] ) ? $instance['playlists'] : 'all';
		$count 			= ! empty( $instance['count'] ) ? $instance['count'] : mgl_tubelab_option('count', 2);
		$cols 			= ! empty( $instance['cols'] ) ? $instance['cols'] : mgl_tubelab_option('cols', 1);
		$mode			= ! empty( $instance['mode'] ) ? $instance['mode'] : mgl_tubelab_option('mode', 'lightbox');
		$template		= ! empty( $instance['template'] ) ? $instance['template'] : mgl_tubelab_option('template', 'default');
		$pagination		= ! empty( $instance['pagination'] ) ? $instance['pagination'] : 'true';
		$display		= ! empty( $instance['display'] ) ? $instance['display'] : mgl_tubelab_option('display', 'title,description,meta');
		$size			= ! empty( $instance['size'] ) ? $instance['size'] : mgl_tubelab_option('size', 'medium');
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'user' ); ?>"><?php _e( 'User', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'user' ); ?>" name="<?php echo $this->get_field_name( 'user' ); ?>" type="text" value="<?php echo esc_attr( $user ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'channel_id' ); ?>"><?php _e( 'Channel ID', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'channel_id' ); ?>" name="<?php echo $this->get_field_name( 'channel_id' ); ?>" type="text" value="<?php echo esc_attr( $channel_id ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'playlists' ); ?>"><?php _e( 'Playlists', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'playlists' ); ?>" name="<?php echo $this->get_field_name( 'playlists' ); ?>" type="text" value="<?php echo esc_attr( $playlists ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e( 'Count', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" type="text" value="<?php echo esc_attr( $count ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'cols' ); ?>"><?php _e( 'Columns', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<?php 
				mgl_tubelab_print_select(
					mgl_tubelab_cols(), 
					esc_attr( $cols ), 
					$this->get_field_id( 'cols' ), 
					$this->get_field_name( 'cols' )
				);
			?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mode' ); ?>"><?php _e( 'Mode', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<?php 
				mgl_tubelab_print_select(
					mgl_tubelab_modes(), 
					esc_attr( $mode ), 
					$this->get_field_id( 'mode' ), 
					$this->get_field_name( 'mode' )
				);
			?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'template' ); ?>"><?php _e( 'Template', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<?php 
				mgl_tubelab_print_select(
					mgl_tubelab_templates(true), 
					esc_attr( $template ), 
					$this->get_field_id( 'template' ), 
					$this->get_field_name( 'template' )
				);
			?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'pagination' ); ?>"><?php _e( 'Pagination', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<?php 
				mgl_tubelab_print_select(
					array(
						'true' 	=> __('Yes', MGL_TUBELAB_DOMAIN),
						'false'	=> __('No', MGL_TUBELAB_DOMAIN)
					), 
					esc_attr( $pagination ), 
					$this->get_field_id( 'pagination' ), 
					$this->get_field_name( 'pagination' )
				);
			?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'display' ); ?>"><?php _e( 'Display', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'display' ); ?>" name="<?php echo $this->get_field_name( 'display' ); ?>" type="text" value="<?php echo esc_attr( $display ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'size' ); ?>"><?php _e( 'Thumbnail size', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<?php 
				mgl_tubelab_print_select(
					mgl_tubelab_sizes(), 
					esc_attr( $size ), 
					$this->get_field_id( 'size' ), 
					$this->get_field_name( 'size' )
				);
			?>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		foreach ($new_instance as $key => $value) {
			$instance[$key] = ( ! empty( $new_instance[$key] ) ) ? strip_tags( $new_instance[$key] ) : '';
		}

		return $instance;
	}

} // class Tubelab_Channel_Widget

register_widget( 'Tubelab_Channel_Widget' );