<?php

/**
 * Adds Tubelab_Video_Widget widget.
 */
class Tubelab_Video_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'tubelab_video_widget', // Base ID
			__( 'Tubelab Video', MGL_TUBELAB_DOMAIN ), // Name
			array( 'description' => __( 'By MaGeek Lab', MGL_TUBELAB_DOMAIN ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		$shortcodeAttributes = '';
		foreach( $instance as $argKey => $argVal ){
            if( is_array($argVal)) { continue; }
            $shortcodeAttributes .= " $argKey" . '="'. (string)$argVal . '"';
        }

        echo do_shortcode( '[mgl_tubelab_video ' . $shortcodeAttributes . ' ]' );
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title 			= ! empty( $instance['title'] ) ? $instance['title'] : __( 'Tubelab Video', MGL_TUBELAB_DOMAIN );
		$video_id 		= ! empty( $instance['video_id'] ) ? $instance['video_id'] : '';
		$mode			= ! empty( $instance['mode'] ) ? $instance['mode'] : mgl_tubelab_option('mode', 'lightbox');
		$template		= ! empty( $instance['template'] ) ? $instance['template'] : mgl_tubelab_option('template', 'default');
		$display		= ! empty( $instance['display'] ) ? $instance['display'] : mgl_tubelab_option('display', 'title,description,meta');
		$size			= ! empty( $instance['size'] ) ? $instance['size'] : mgl_tubelab_option('size', 'medium');
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'video_id' ); ?>"><?php _e( 'Video ID', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'video_id' ); ?>" name="<?php echo $this->get_field_name( 'video_id' ); ?>" type="text" value="<?php echo esc_attr( $video_id ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mode' ); ?>"><?php _e( 'Mode', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<?php 
				mgl_tubelab_print_select(
					mgl_tubelab_modes(), 
					esc_attr( $mode ), 
					$this->get_field_id( 'mode' ), 
					$this->get_field_name( 'mode' )
				);
			?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'template' ); ?>"><?php _e( 'Template', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<?php 
				mgl_tubelab_print_select(
					mgl_tubelab_templates(true), 
					esc_attr( $template ), 
					$this->get_field_id( 'template' ), 
					$this->get_field_name( 'template' )
				);
			?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'display' ); ?>"><?php _e( 'Display', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'display' ); ?>" name="<?php echo $this->get_field_name( 'display' ); ?>" type="text" value="<?php echo esc_attr( $display ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'size' ); ?>"><?php _e( 'Thumbnail size', MGL_TUBELAB_DOMAIN ); ?>:</label> 
			<?php 
				mgl_tubelab_print_select(
					mgl_tubelab_sizes(), 
					esc_attr( $size ), 
					$this->get_field_id( 'size' ), 
					$this->get_field_name( 'size' )
				);
			?>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		foreach ($new_instance as $key => $value) {
			$instance[$key] = ( ! empty( $new_instance[$key] ) ) ? strip_tags( $new_instance[$key] ) : '';
		}

		return $instance;
	}

} // class Tubelab_Video_Widget

register_widget( 'Tubelab_Video_Widget' );