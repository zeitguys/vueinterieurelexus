(function($){
	$(document).ready(function(){
		$('.mgl_tubelab').fitVids();

		$('.mgl_tubelab_single .mgl_tubelab_lightbox').magnificPopup({
	       type: 'iframe',
	       iframe: {
               patterns: {
                   youtube: {
                       index: 'youtube.com', 
                       id: 'v=', 
                       src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1&showinfo=0'
                    }
               }
           }
	    });


		    
		var configureLightBox = function( $container ){

			$('.mgl_tubelab').fitVids();

			$container.each(function() { // the containers for all your galleries
			    $(this).magnificPopup({
			        delegate: '.mgl_tubelab_lightbox', // the selector for gallery item
			        type: 'iframe',
			        iframe: {
		               patterns: {
		                   youtube: {
		                       index: 'youtube.com', 
		                       id: 'v=', 
		                       src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1&showinfo=0'
		                    }
		               }
		            },
			        gallery: {
			          enabled:true
			        }
			    });
			});
		}

		var loadContent = function( $container, pageToken ){
			var data = {
				'action': 'mgl_tubelab_loadContent',
				'parameters': $container.data('mgl-tubelab-parameters'),
				'containerType': $container.data('mgl-tubelab-container-type'),
			};
			
			if( pageToken != undefined ){ data.pageToken = pageToken; }

			var $containerContent = $('.mgl_tubelab_content', $container);

			$containerContent.css( 'opacity' , '0.2' );

			$spinner = '<div class="mgl_tubelab_spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';
			$containerContent.append( $spinner );

			jQuery.post(ajax_object.ajax_url, data, function(response) {
				
				try {
					response = $.parseJSON( response );		
					$containerContent.empty().html( response.content );	
				} catch(err) {
					$containerContent.empty().html( '<div class="mgl_tubelab-alert">'+response+'</div>' );	
				}
				
	
				$container.data('mgl-tubelab-nextid', response.nextId );
				$container.data('mgl-tubelab-previd', response.prevId );

				if( response.prevId == undefined ){
					$container.find('.mgl_tubelab_pagination_prev').hide();
				}else{
					$container.find('.mgl_tubelab_pagination_prev').show();
				}

				if( response.nextId == undefined ){
					$container.find('.mgl_tubelab_pagination_next').hide();
				}else{
					$container.find('.mgl_tubelab_pagination_next').show();
				}

				configureLightBox( $container );
				$containerContent.css('opacity' , '1');
			});
		}

		$('.mgl_tubelab_container').each(function(){
			var $container = $( this );
			
			$container.find('.mgl_tubelab_pagination_prev, .mgl_tubelab_pagination_next').hide();
			
			$('.mgl_tubelab_pagination_next', $container ).click( function( e ){
				loadContent( $container, $container.data('mgl-tubelab-nextid') );				
				return false;
			});

			$('.mgl_tubelab_pagination_prev', $container ).click( function( e ){
				loadContent( $container, $container.data('mgl-tubelab-previd') );				
				return false;
			});

			loadContent( $container );
		});

		$('.mgl_tubelab_channel').each(function(){
			var $channel 			= $( this );
			var $playlistsNav 		= $('.mgl_tubelab_channel_playlist_nav', $channel);
			var $playlistsContainer = $('.mgl_tubelab_channel_playlists', $channel);

			$('.mgl_tubelab_channel_playlist_nav_item', $playlistsNav ).click( function( e ){
				var currentPlaylist = $(this).attr('href');
				
				$('.mgl_tubelab_channel_playlist_nav_item', $playlistsNav ).removeClass('mgl_tubelab_channel_playlist_nav_item-active');
				$('.mgl_tubelab_channel_playlist', $playlistsContainer ).removeClass('mgl_tubelab_channel_playlist-active');
				
				$(this).addClass('mgl_tubelab_channel_playlist_nav_item-active');
				$(currentPlaylist, $playlistsContainer ).addClass('mgl_tubelab_channel_playlist-active');

				$playlistsNav.removeClass('mgl_tubelab_channel_playlist_nav-open');

				return false;
			});

			$('.mgl_tubelab_channel_playlist_nav_toggle', $playlistsNav).click(function(e){
				
				if( $playlistsNav.hasClass('mgl_tubelab_channel_playlist_nav-open') ) {
					$playlistsNav.removeClass('mgl_tubelab_channel_playlist_nav-open');
				} else {
	    			$playlistsNav.addClass('mgl_tubelab_channel_playlist_nav-open');
				}

	    		return false;
	   		});


		});

	});
})(jQuery);