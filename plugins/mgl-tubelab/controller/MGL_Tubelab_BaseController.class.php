<?php
abstract class MGL_Tubelab_BaseController {

    protected   $repositoryManager,
                $count,
                $cols,
                $pagination,
                $user,
                $mode,
                $size;

    public      $channelId,
                $channelInfo,
                $template,
                $display;

    public function __construct( $atts ){
        $this->repositoryManager = new MGL_Tubelab_RepositoryManager();
        
        $this->count        = $atts['count'];
        $this->cols         = $atts['cols'];
        $this->template     = $atts['template'];
        $this->pagination   = $atts['pagination'];
        $this->user         = $atts['user'];
        $this->size         = $atts['size'];
        $this->mode         = $atts['mode'];
        $this->channelId    = $atts['channel_id'];
        $this->display      = $atts['display'];
        
    }

    public function loadScriptsAndStyles(){
        wp_enqueue_style('mgl_tubelab');
        wp_enqueue_script('mgl_tubelab_loader');
        
        wp_enqueue_script('mgl_tubelab_fitvids');
        
        wp_enqueue_style('mgl_tubelab_magnific_popup');
        wp_enqueue_script('mgl_tubelab_magnific_popup');

        $tubelabOptions = get_option('mgl_tubelab', array(), true);
        $loadjQuery = (int) ( isset( $tubelabOptions['configuration']['jquery']) ) ? $tubelabOptions['configuration']['jquery'] : 1;
        if(!is_admin() && $loadjQuery) {
            wp_enqueue_script("jquery"); 
        }
    }

    public function includeTemplate( $templatePart, $accessibleVars ){
        extract( $accessibleVars );

        $themeTemplateUrl   = get_template_directory() . '/tubelab/' . $this->template . '/' . $templatePart .'.php';
        $pluginTemplateUrl  = MGL_YOUTUBE_INCLUDE_BASE_PATH . '/templates/' . $this->template . '/' . $templatePart .'.php';
        $defaultTemplateUrl = MGL_YOUTUBE_INCLUDE_BASE_PATH . '/templates/default/' . $templatePart .'.php';

        if( file_exists( $themeTemplateUrl ) ){
            include( $themeTemplateUrl );
        }elseif( file_exists( $pluginTemplateUrl ) ){
            include( $pluginTemplateUrl );
        } else {
            include( $defaultTemplateUrl );
        }
    }

    public function renderTemplate( $template, $accessibleVars){
        ob_start();
        $this->includeTemplate( $template, $accessibleVars );
        return ob_get_clean();
    }

    abstract function getVideos();
    abstract function render();
    abstract function renderContent();

}