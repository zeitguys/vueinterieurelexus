<?php
class MGL_Tubelab_ChannelController extends MGL_Tubelab_BaseController {

    private $videos;

    public  $channelPlaylists,
            $givenPlaylists,
            $playListRepository,
            $mini;

    public function __construct( $atts ){
        parent::__construct( $atts );

        $this->givenArgs        = $atts;
        $this->givenPlaylists   = $atts['playlists'];
        $this->mini             = $atts['mini'];
        
        $this->playListRepository = $this->repositoryManager->getRepository( MGL_Tubelab_RepositoryManager::PLAYLIST );
        
        $this->configureChannelInfo();

    }

    public function configureChannelInfo(){
        if($this->channelId == '') {
            $this->channelId   = $this->playListRepository->getChannelIdByUsername( $this->user );
        }
        $this->channelInfo = $this->playListRepository->getChannelInfoById( $this->channelId );

        $this->channelPlaylists = $this->getPlaylistWithAtts();
    }

    public function getVideos(){
        $videoIds = $this->playListRepository->getPlayListItemsIdsByPlayListId( $this->playListId );
        return $this->playListRepository->getVideosByIdArray( $videoIds );

    }

    private function checkPlaylistShow( $playlist ) {

        if( $this->givenPlaylists == 'all' ) {
            return true;
        } 

        $givenPlaylists = explode( ',', $this->givenPlaylists );

        if( in_array($playlist, $givenPlaylists) ) {
            return true;
        } else {
            return false;
        }
    }

    public function getPlaylistWithAtts() {
        $playlists = array();
        
        $availablePlaylists = $this->channelInfo->contentDetails->relatedPlaylists;
        $additionalPlaylists = $this->playListRepository->getPLaylistsByChannelId( $this->channelId, $this->givenPlaylists );
        
        $dummyArray = array();

        foreach (array('uploads', 'favorites', 'likes') as $key) {
            if( isset($availablePlaylists->$key ) ) {
                $dummyArray[$key] = $availablePlaylists->$key;
            }
        }
        $availablePlaylists = $dummyArray;
        
        foreach ($availablePlaylists as $key => $playlist) {

            if(! $this->checkPlaylistShow ( $key ) ) {
                continue;
            }

            $channelPlaylist = array(); 
            switch ($key) {
                case 'likes':
                    $channelPlaylist['title'] = __('Likes', MGL_TUBELAB_DOMAIN);
                    break;
                case 'favorites':
                    $channelPlaylist['title'] = __('Favorites', MGL_TUBELAB_DOMAIN);
                    break;
                case 'uploads':
                    $channelPlaylist['title'] = __('Videos', MGL_TUBELAB_DOMAIN);
                    break;
                default:
                    $channelPlaylist['title'] = $key;
                    break;
            }

            $playlistArgs = $this->givenArgs;
            $playlistArgs['value'] = $playlist;
            $playlistArgs['type']   = 'playlist';
            $channelPlaylist['atts'] = http_build_query( $playlistArgs );

            
            $playlists[$playlist] =  $channelPlaylist;
        }
        
        foreach ($additionalPlaylists as $key => $playlist) {

            if(! $this->checkPlaylistShow ( $playlist->id ) ) {
                continue;
            }

            $channelPlaylist = array(); 
            
            $channelPlaylist['title'] = $playlist->snippet->title;

            $playlistArgs = $this->givenArgs;
            $playlistArgs['value'] = $playlist->id;
            $playlistArgs['type']   = 'playlist';
            $channelPlaylist['atts'] = http_build_query( $playlistArgs );

            $playlists[$playlist->id] =  $channelPlaylist;
        }

        return $playlists;
    }

    public function render(){
        $this->loadScriptsAndStyles();

        return $this->renderTemplate( 'channel', array(
            'template'      => $this->template,
            'mini'          => $this->mini,
            'cols'          => $this->cols,
            'channelInfo'   => $this->channelInfo,
            'playlists'     => $this->channelPlaylists,
            'pagination'    => $this->pagination
        ));
    }

    public function renderContent(){
        $resultHtml = '';
        foreach ($this->videos as $key => $video) {
            $resultHtml .= $this->renderTemplate('loop', array( 'video' => $video ) );
        }
        return false;
        return $resultHtml;
    }
}