<?php
class MGL_Tubelab_PlaylistController extends MGL_TubeLab_BaseController {

    private $type,
            $value,
            $videos,
            $response;

    public  $nextId,
            $prevId,
            $pageToken,
    		$playlistId,
            $playListRepository;

    public function __construct( $atts ){
        parent::__construct( $atts );
        $this->givenArgs = $atts;

        $this->type     = $atts['type'];
        $this->value    = $atts['value'];
        
        if(isset($atts['pageToken'])) {
            $this->pageToken  = $atts['pageToken'];
        } else {
            $this->pageToken  = '';
        }
        
        $this->playListRepository = $this->repositoryManager->getRepository( MGL_Tubelab_RepositoryManager::PLAYLIST );
        $this->playListId  = $this->getPlayListIdByType();
        $this->response = $this->playListRepository->getResponseByPlayListId( $this->playListId, $this->count, $this->pageToken );
        
        $this->videos = $this->getVideos();
        $this->configureNavigation();
    }

    private function getPlayListIdByType(){
        switch($this->type) {
            case 'channel':
                
                if($this->channelId == '') {
                    $this->channelId   = $this->playListRepository->getChannelIdByUsername( $this->user );
                }
                $this->channelInfo = $this->playListRepository->getChannelInfoById( $this->channelId );
                
                if( $this->value == 'likes' ) {
                    return $this->channelInfo->contentDetails->relatedPlaylists->likes;
                } elseif( $this->value == 'favorites' ) {
                    return $this->channelInfo->contentDetails->relatedPlaylists->favorites;
                } else {
                    return $this->channelInfo->contentDetails->relatedPlaylists->uploads;
                }

                break;
            case 'playlist':
                
                return $this->value;
                
                break;

            default:
                throw new Exception('Playlist type not valid, use "channel" or "playlist"');
                break;
        }
    }

    public function getVideos(){
        return $this->playListRepository->getPlayListVideosByPlayListId( $this->response );
    }

    public function render(){

        $this->loadScriptsAndStyles();

        return $this->renderTemplate( 'container', array(
            'args'       => $this->getUrlEncodedArgs(),
            'template'   => $this->template,
            'cols'       => $this->cols,
            'pagination' => $this->pagination
        ));
    }

    public function renderContent(){
        $resultHtml = '';
        if( count($this->videos) === 0) return '<div class="mgl_tubelab_empty">'.__('No videos here', MGL_TUBELAB_DOMAIN).'</div>';
        foreach ($this->videos as $key => $video) {
            $resultHtml .= $this->renderTemplate('loop', array( 
                'video'     => $video,
                'mode'      => $this->mode,
                'size'      => $this->size,
                'display'   => explode(',', $this->display)
            ));
        }

        return $resultHtml;
    }

    public function configureNavigation(){

        if( isset($this->response->nextPageToken) ) {
            $this->nextId = $this->response->nextPageToken;
        }

        if( isset($this->response->prevPageToken) ) {
            $this->prevId = $this->response->prevPageToken;
        }
    }

    public function getUrlEncodedArgs(){
        return http_build_query( $this->givenArgs );
    }
}