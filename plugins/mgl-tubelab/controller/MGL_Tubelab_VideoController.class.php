<?php
class MGL_Tubelab_VideoController extends MGL_Tubelab_BaseController {
	
	public 	$videoId,
			$video,
			$channel;

	private $videoRepository;

	public function __construct( $atts ){
        parent::__construct( $atts );

        $this->videoId = $atts['video_id'];

        $this->videoRepository = $this->repositoryManager->getRepository( MGL_Tubelab_RepositoryManager::VIDEO );

        $this->response = $this->videoRepository->getVideoById( $this->videoId );
        $this->channel 	= $this->videoRepository->getChannelById( $this->response->snippet->channelId );

        $this->video = $this->getVideos();
    }

    public function render(){

        $this->loadScriptsAndStyles();

        return $this->renderTemplate( 'single', array(
        	'template'	=> $this->template,
            'video'     => $this->video,
            'channel'	=> $this->channel,
            'mode'		=> $this->mode,
            'size'		=> $this->size,
            'display'   => explode(',', $this->display)
        ));
    }
	public function getVideos(){       
        return $this->response;
	}
    public function renderContent(){}
}