<?php $miniClass = ( $mini === 'true' ) ? ' mgl_tubelab_channel_mini' : ''; ?>
<div class="mgl_tubelab mgl_tubelab_channel mgl_tubelab_template_<?php echo $template.$miniClass; ?>">
    <div class="mgl_tubelab_banner" style="background-image: url('<?php echo $channelInfo->brandingSettings->image->bannerImageUrl; ?>');" >
        <div class="mgl_tubelab_channel_info">
            <span class="mgl_tubelab_user_avatar">
                <img src="<?php echo $channelInfo->snippet->thumbnails->medium->url; ?>" />
            </span>
            <div class="mgl_tubelab_channel_title">
                <?php echo $channelInfo->snippet->title; ?>
                <div class="mgl_tubelab_channel_button mgl_tubelab_subscribe_button">
                    <script src="https://apis.google.com/js/platform.js"></script>
                    <div class="g-ytsubscribe" data-channelid="<?php echo $channelInfo->id; ?>" data-layout="default" data-count="default"></div>
                </div>
            </div>
            <div class="mgl_tubelab_channel_statistics">
                <div><?php printf( __( '%1$s videos', MGL_TUBELAB_DOMAIN ), $channelInfo->statistics->videoCount ); ?></div>
                <div><?php printf( __( '%1$s views', MGL_TUBELAB_DOMAIN ), $channelInfo->statistics->viewCount ); ?></div>
                <div><?php printf( __( '%1$s subscribers', MGL_TUBELAB_DOMAIN ), $channelInfo->statistics->subscriberCount ); ?></div>
            </div>
        </div>
    </div>
    <?php if( count($playlists) > 0 ): ?>
        <nav class="mgl_tubelab_channel_playlist_nav">
            <a href="#" class="mgl_tubelab_channel_playlist_nav_toggle"><?php _e('Show playlists', MGL_TUBELAB_DOMAIN); ?></a>
            <ul>
            <?php 
            $playlist_count = 0;

            foreach ($playlists as $key => $playlist) { 

                $playlist_count++;
                $playlist_class = 'mgl_tubelab_channel_playlist_nav_item';
                if($playlist_count == 1) { $playlist_class .= ' mgl_tubelab_channel_playlist_nav_item-active'; }
            ?>
                <li><a class="<?php echo $playlist_class; ?>" href="#tubelab_channel_playlist_<?php echo $key; ?>" title="<?php echo $playlist['title']; ?>"><?php echo $playlist['title']; ?></a></li>
            <?php } ?>
            </ul>
        </nav>
    <?php endif; ?>
    <div class="mgl_tubelab_channel_playlists">
        <?php 
        $playlist_count = 0;

        foreach ($playlists as $key => $playlist) { 

            $playlist_count++;
            $playlist_class = 'mgl_tubelab_channel_playlist';

            if($playlist_count == 1) { $playlist_class .= ' mgl_tubelab_channel_playlist-active'; }

        ?>
            <div id="tubelab_channel_playlist_<?php echo $key; ?>" class="<?php echo $playlist_class; ?>">
                <!-- TODO: Encapsular esto dentro de container -->
                <div class="mgl_tubelab mgl_tubelab_container mgl_tubelab_template_<?php echo $template; ?>" data-mgl-tubelab-parameters="<?php echo $playlist['atts']; ?>">
                    <div class="mgl_tubelab mgl_tubelab_content mgl_tubelab_content-cols<?php echo $cols; ?>">
                    </div>
                    <?php if( $pagination != 'false' ): ?>
                        <div class="mgl_tubelab_pagination">
                            <div class="mgl_tubelab_pagination_item mgl_tubelab_pagination_item_prev">
                                <a href="#" class="mgl_tubelab_pagination_prev"><?php _e('Previous page'); ?></a>
                            </div>
                            <div class="mgl_tubelab_pagination_item mgl_tubelab_pagination_item_next">
                                <a href="#" class="mgl_tubelab_pagination_next"><?php _e('Next page'); ?></a>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <!-- END TODO -->
            </div>
        <?php } ?>
    </div>
</div><!-- END .mgl_youtubeGallery_channel -->