<div class="mgl_tubelab mgl_tubelab_container mgl_tubelab_template_<?php echo $template; ?>" data-mgl-tubelab-parameters="<?php echo $args; ?>">
	<div class="mgl_tubelab mgl_tubelab_content mgl_tubelab_content-cols<?php echo $cols; ?>">
	</div>
	<?php if( $pagination != 'false' ): ?>
		<div class="mgl_tubelab_pagination">
			<div class="mgl_tubelab_pagination_item mgl_tubelab_pagination_item_prev">
				<a href="#" class="mgl_tubelab_pagination_prev"><?php _e( 'Previous page' , MGL_TUBELAB_DOMAIN ); ?></a>
			</div>
			<div class="mgl_tubelab_pagination_item mgl_tubelab_pagination_item_next">
				<a href="#" class="mgl_tubelab_pagination_next"><?php _e( 'Next page' , MGL_TUBELAB_DOMAIN ); ?></a>
			</div>
		</div>
	<?php endif; ?>
</div>