<?php
	$tubelab_video_class = 'mgl_tubelab_video';
	$target = '';

	if($mode == 'lightbox'): $tubelab_video_class .= ' mgl_tubelab_single_lightbox'; endif;
	if($mode == 'direct'): $target .= ' target="_blank"'; endif;
?>
<div class="mgl_tubelab mgl_tubelab_single mgl_tubelab_template_<?php echo $template; ?>">
	
	<?php mgl_tubelab_print_video($video, $mode, $size); ?>
		
	<?php if( mgl_tubelab_display($display, array('title','channel','description') ) ): ?>

		<div class="mgl_tubelab_single_info">
			
			<?php if( mgl_tubelab_display($display, 'title') ): // Print video title ?>		
				
				<div class="mgl_tubelab_single_title">
					<?php echo $video->snippet->title; ?>
					<div class="mgl_tubelab_single_published">
						<?php echo mgl_tubelab_get_published($video->snippet->publishedAt); ?>
					</div>
				</div>

			<?php endif; ?>

			<?php if( mgl_tubelab_display($display, 'channel') ): // Print video title ?>	
				
				<div class="mgl_tubelab_single_channel">
					<span class="mgl_tubelab_single_channel_avatar">
						<img src="<?php echo $channel->snippet->thumbnails->default->url; ?>" />
					</span>
					<span class="mgl_tubelab_single_channel_title"><?php echo $channel->snippet->title; ?></span>
					<div class="mgl_tubelab_subscribe_button">
						<script src="https://apis.google.com/js/platform.js"></script>
			            <div class="g-ytsubscribe" data-channelid="<?php echo $channel->id; ?>" data-layout="default" data-count="default"></div>
		            </div>
				</div>

			<?php endif; ?>
			<?php if( mgl_tubelab_display($display, 'description') ): // Print video title ?>

				<div class="mgl_tubelab_single_description">
					<?php echo mgl_tubelab_rich_text($video->snippet->description); ?>
				</div>

			<?php endif; ?>

		</div>

	<?php endif;  ?>

	<?php if( mgl_tubelab_display($display, 'meta') ): // Print video title ?>
		
		<div class="mgl_tubelab_single_metas">
	    	<div class="mgl_tubelab_single_meta">
				<span class="mgl_tubelab_single_meta_icon"><i class="mgl_tubelab-icon-eye"></i></span>
	    		<?php echo $video->statistics->viewCount; ?>
	    	</div>
	    	<div class="mgl_tubelab_single_meta">
				<span class="mgl_tubelab_single_meta_icon"><i class="mgl_tubelab-icon-heart"></i></span>   		
	    		<?php echo $video->statistics->likeCount; ?>
	    	</div>
	    	<div class="mgl_tubelab_single_meta">
	    		<span class="mgl_tubelab_single_meta_icon"><i class="mgl_tubelab-icon-comment"></i></span>
	    		<?php echo $video->statistics->commentCount; ?>
	    	</div>
		</div>

	<?php endif; ?>
</div>