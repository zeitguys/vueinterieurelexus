<div class="mgl_tubelab_col">
	<div class="mgl_tubelab_item">
		
		<?php mgl_tubelab_print_video($video, $mode, $size); ?>
		
		<?php if( mgl_tubelab_display($display, array('title', 'description')) ): ?>
			
		
			<div class="mgl_tubelab_item_info">
				
				<?php if( mgl_tubelab_display($display, 'title') ): // Print video title ?>
					
					<div class="mgl_tubelab_item_title"><?php echo $video->snippet->title; ?></div>
				
				<?php endif; ?>
				
				<?php if( mgl_tubelab_display($display, 'description') ): // Print video description ?>
					
					<?php echo mgl_tubelab_cut_string($video->snippet->description); ?>

				<?php endif; ?>

			</div>
		
		<?php endif; ?>


		<?php if( mgl_tubelab_display($display, 'meta') ): // Print video meta ?>

			<div class="mgl_tubelab_item_metas">
		    	<div class="mgl_tubelab_item_meta">
					<span class="mgl_tubelab_item_meta_icon"><i class="mgl_tubelab-icon-eye"></i></span>
		    		<?php echo $video->statistics->viewCount; ?>
		    	</div>
		    	<div class="mgl_tubelab_item_meta">
					<span class="mgl_tubelab_item_meta_icon"><i class="mgl_tubelab-icon-heart"></i></span>		
		    		<?php echo $video->statistics->likeCount; ?>
		    	</div>
		    	<div class="mgl_tubelab_item_meta">
		    		<span class="mgl_tubelab_item_meta_icon"><i class="mgl_tubelab-icon-comment"></i></span>
		    		<?php echo $video->statistics->commentCount; ?>
		    	</div>
			</div>

		<?php endif; ?>

	</div>
</div>