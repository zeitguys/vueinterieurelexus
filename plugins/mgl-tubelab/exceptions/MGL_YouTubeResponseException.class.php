<?php
/**
 * Define a custom exception class
 */
class MGL_YouTubeResponseException extends Exception
{
    private $response;

    // Redefine the exception so message isn't optional
    public function __construct( $response ) {
        // some code
        $this->response = json_decode($response['body']);

        $errors = '';

        foreach ($this->response->error->errors as $error) {
            $errors .= 'Error: '.$error->domain.' - '.$error->reason.'<br />';
        }

        $this->code     = (int) $this->response->error->code;
        $this->message  = 'MGL Youtube: ' .$this->response->error->message.' <br />'.$errors;
    
        // make sure everything is assigned properly
        parent::__construct( $this->message, $this->code);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function customFunction() {
        echo "A custom function for this type of exception\n";
    }
}