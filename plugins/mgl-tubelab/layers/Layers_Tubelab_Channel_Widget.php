<?php 
/**
 * Channel Widget
 *
 * This file is used to register and display the Layers - Channel widget.
 *
 * @package Layers
 * @since Layers 1.0.0
 */
if( !class_exists( 'Layers_Tubelab_Channel_Widget' ) ) {
 
class Layers_Tubelab_Channel_Widget extends Layers_Widget {
 
        /**
        *  Widget construction
        */
        function Layers_Tubelab_Channel_Widget(){

            $this->widget_title = __( 'Tubelab Channel' , MGL_TUBELAB_DOMAIN );
            $this->widget_id = 'tubelab-channel';

            /* Widget settings. */ 
 
            $widget_ops = array( 
                  'classname' => 'mgl-layers-' . $this->widget_id .'-widget', 
                  'description' => __( 'Show a YouTube channel with Tubelab', MGL_TUBELAB_DOMAIN )
            );
            
            /* Widget control settings. */
            $control_ops = array( 
                  'width' => '660', 
                  'height' => NULL, 
                  'id_base' => 'layers-widget-' . $this->widget_id 
            );

            /* Create the widget. */ 
            $this->WP_Widget( 
                    'layers-widget-' . $this->widget_id , 
                     $this->widget_title, 
                     $widget_ops, 
                     $control_ops 
            );

            /* Setup Widget Defaults */ 
            $this->defaults = array ( 
                // Our defaults will sit here. 
            );
        }
 
        /**
        *  Widget front end display
        */
        function widget( $args, $instance ) {
            // Turn $args array into variables.
            extract( $args );
         
            // $instance Defaults
            $instance_defaults = $this->defaults;
         
            // If we have information in this widget, then ignore the defaults
            if( !empty( $instance ) ) $instance_defaults = array();
         
            // Parse $instance
            $widget = wp_parse_args( $instance, $instance_defaults );

            if( !empty( $widget['design'][ 'background' ] ) ) {
                layers_inline_styles(
                    '#' . $widget_id,
                    'background',
                    array(
                        'background' => $widget['design'][ 'background' ]
                    )
                );
            }

            $this->apply_widget_advanced_styling( $widget_id, $widget );

            ?>

            <section class="widget row content-vertical-massive <?php echo $this->check_and_return( $widget , 'design', 'advanced', 'customclass' ) ?> <?php echo $this->get_widget_spacing_class( $widget ); ?>" id="<?php echo $widget_id; ?>">
                <div class="row <?php echo $this->get_widget_layout_class( $widget ); ?> <?php echo $this->check_and_return( $widget , 'design', 'liststyle' ); ?>">

                
                <!-- Widget HTML will go here -->
                <?php
                    $shortcodeAttributes = '';
                    foreach( $widget as $argKey => $argVal ){
                        if( is_array($argVal)) { continue; }
                        $shortcodeAttributes .= " $argKey" . '="'. (string)$argVal . '"';
                    }

                    echo do_shortcode( '[mgl_tubelab_channel ' . $shortcodeAttributes . ' ]' );

                ?>
                </div>
            </section>
            <?php
        }
 
        /**
        *  Widget form
        *
        * We use regulage HTML here, it makes reading the widget much easier 
        * than if we used just php to echo all the HTML out.
        *
        */
        function form( $instance ){

            $instance_defaults = $this->defaults;
 
            // If we have information in this widget, then ignore the defaults
            if( !empty( $instance ) ) $instance_defaults = array();
         
            // Parse $instance
            $instance = wp_parse_args( $instance, $instance_defaults );
         
            extract( $instance, EXTR_SKIP );
            
            $design_bar_components = apply_filters(
                'layers_' . $this->widget_id . '_widget_design_bar_components' ,
                array(
                    'custom',
                    'layout', 
                    'background',
                    'advanced'
                )
            );

            $design_bar_custom_components = apply_filters(
               'layers_' . $this->widget_id . '_widget_design_bar_custom_components' ,
                   array(
                    'columns' => array(
                       'icon-css' => 'icon-columns',
                       'label' => __( 'Columns & count', MGL_TUBELAB_DOMAIN ),
                       'elements' => array(
                            'count' => array(
                                 'type' => 'number',
                                 'name' => $this->get_field_name( 'count' ) ,
                                 'id' => $this->get_field_id( 'count' ) ,
                                 'min' => 0,
                                 'max' => 50,
                                 'value' => ( isset( $count ) ) ? $count : mgl_tubelab_option('count', 12),
                                 'label' => __( 'Count' , MGL_TUBELAB_DOMAIN )
                             ),
                            'cols' => array(
                                'type' => 'select',
                                'name' => $this->get_field_name( 'cols' ) ,
                                'id' => $this->get_field_id( 'cols' ) ,
                                'value' => ( isset( $cols ) ) ? $cols : mgl_tubelab_option('cols', 4),
                                'label' => __( 'Columns' , MGL_TUBELAB_DOMAIN ),
                                'options' => mgl_tubelab_cols()
                            ),
                        )
                    ),
                    'display' => array(
                       'icon-css' => 'icon-display',
                       'label' => __( 'Display', MGL_TUBELAB_DOMAIN ),
                       'elements' => array(
                         'display' => array(
                               'type' => 'text',
                               'name' => $this->get_field_name( 'display' ) ,
                               'id' => $this->get_field_id( 'display' ) ,
                               'value' => ( isset( $display ) ) ? $display : mgl_tubelab_option('display', 'title,description,meta'),
                               'label' => __( 'Item display' , MGL_TUBELAB_DOMAIN )
                         ),
                         'size' => array(
                               'type' => 'select',
                               'name' => $this->get_field_name( 'size' ) ,
                               'id' => $this->get_field_id( 'size' ) ,
                               'value' => ( isset( $size ) ) ? $size : mgl_tubelab_option('size', 'medium'),
                               'label' => __( 'Thumbnail size' , MGL_TUBELAB_DOMAIN ),
                               'options' => mgl_tubelab_sizes(true)
                         ),
                         'pagination' => array(
                               'type' => 'select',
                               'name' => $this->get_field_name( 'pagination' ) ,
                               'id' => $this->get_field_id( 'pagination' ) ,
                               'value' => ( isset( $pagination ) ) ? $pagination : 'true',
                               'label' => __( 'Show Pagination' , MGL_TUBELAB_DOMAIN ),
                               'options' => array(
                                    'true' => 'Yes',
                                    'false'=> 'No'
                                )
                         ),
                       )
                   )
               )
            );

            $this->design_bar(
                'side', // CSS Class Name
                array(
                    'name' => $this->get_field_name( 'design' ),
                    'id' => $this->get_field_id( 'design' ),
                ), // Widget Object
                $instance, // Widget Values
                $design_bar_components, // Standard Components
                $design_bar_custom_components // Add-on Components
            );

            

             ?>
            <div class="layers-container-large">
                <?php $this->form_elements()->header( 
                   array(
                    'title' =>  __( 'Channel' , MGL_TUBELAB_DOMAIN ),
                    'icon_class' => 'channel'
                   ) 
                ); ?>
                <section class="layers-accordion-section layers-content">
                    <p>Set the username or channel ID to pull the info from, you don't need both</p>
                    <div class="layers-row layers-push-bottom">
                        <p class="layers-form-item">
                            <label for="<?php echo $this->get_field_id( 'user' ); ?>"><?php echo __( 'YouTube username' , MGL_TUBELAB_DOMAIN ); ?></label>
                                <?php echo $this->form_elements()->input(
                                array(
                                    'type' => 'text',
                                    'name' => $this->get_field_name( 'user' ) ,
                                    'id' => $this->get_field_id( 'user' ) ,
                                    'value' => ( isset( $user ) ) ? $user : NULL ,
                                )
                            ); ?>
                        </p>
                     
                        <p class="layers-form-item">
                            <label for="<?php echo $this->get_field_id( 'channel_id' ); ?>"><?php echo __( 'YouTube Channel ID' , MGL_TUBELAB_DOMAIN ); ?></label>
                            <?php echo $this->form_elements()->input(
                                array(
                                    'type' => 'text',
                                    'name' => $this->get_field_name( 'channel_id' ) ,
                                    'id' => $this->get_field_id( 'channel_id' ) ,
                                    'value' => ( isset( $channel_id ) ) ? $channel_id : NULL ,
                                )
                            ); ?>
                        </p>

                        <p class="layers-form-item">
                            <label for="<?php echo $this->get_field_id( 'playlists' ); ?>"><?php echo __( 'Playlists' , MGL_TUBELAB_DOMAIN ); ?></label>
                            <?php echo $this->form_elements()->input(
                                array(
                                    'type' => 'text',
                                    'name' => $this->get_field_name( 'playlists' ) ,
                                    'id' => $this->get_field_id( 'playlists' ) ,
                                    'value' => ( isset( $playlists ) ) ? $playlists : NULL ,
                                )
                            ); ?>
                        </p>

                        <p><?php _e('By default the channel will load all the available playlists, if you want you can specify the Ids sepparated by commas, use "uploads" or "likes" for the uploads playlist and the likes playlist', MGL_TUBELAB_DOMAIN); ?></p>

                        <p class="layers-form-item">
                            <label for="<?php echo $this->get_field_id( 'mode' ); ?>"><?php echo __( 'Mode' , MGL_TUBELAB_DOMAIN ); ?></label>
                            <?php echo $this->form_elements()->input(
                                array(
                                    'type'      => 'select',
                                    'name'      => $this->get_field_name( 'mode' ) ,
                                    'id'        => $this->get_field_id( 'mode' ) ,
                                    'value'     => ( isset( $mode ) ) ? $mode : mgl_tubelab_option('mode', 'lightbox'),
                                    'label'     => __( 'Mode' , MGL_TUBELAB_DOMAIN ),
                                    'options'   => mgl_tubelab_modes()
                                )
                            ); ?>
                        </p>

                        <p class="layers-form-item">
                            <label for="<?php echo $this->get_field_id( 'template' ); ?>"><?php echo __( 'Template' , MGL_TUBELAB_DOMAIN ); ?></label>
                            <?php echo $this->form_elements()->input(
                                array(
                                    'type'      => 'select',
                                    'name'      => $this->get_field_name( 'template' ) ,
                                    'id'        => $this->get_field_id( 'template' ) ,
                                    'value'     => ( isset( $template ) ) ? $template : mgl_tubelab_option('template', 'default'),
                                    'options'   => mgl_tubelab_templates(true)
                                )
                            ); ?>
                        </p>
                        <p><?php printf( __('Set the name of the skin, the possible values are: %1$s. You can also set the name of a custom template', MGL_TUBELAB_DOMAIN) , implode( ', ', mgl_tubelab_templates() ) ); ?></p>
                    </div>
                </section>
            </div>
             <?php
        } // Form
    } // Class
 
    // Register our widget
    register_widget("Layers_Tubelab_Channel_Widget"); 
}
?>