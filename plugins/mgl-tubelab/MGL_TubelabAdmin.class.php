<?php

class MGL_TubelabAdmin {

    const STATUS_MESSAGE_UPDATED = 'updated';
    const STATUS_MESSAGE_ERROR = 'error';
    const STATUS_MESSAGE_SUCCESS = 'success';

    public $flashMessageBag = array(),
           $mglTubelabSettings;

    public function __construct(){
        //Add settings page to Admin menu
        add_action('admin_menu', array( $this, 'addSettingsPageToAdminMenu' ) );

        //Load scripts in MGL Youtube Admin panel
        add_action('admin_enqueue_scripts', array( $this, 'loadSettingsPageCSS' ) );
    }

    public function addSettingsPageToAdminMenu() {  
        add_submenu_page(
            'options-general.php', 
            'Tubelab', 
            'Tubelab', 
            'administrator', 
            'mgl-tubelab', 
            array( $this, 'tubelabSettingsPage' ) 
        ); 
    }//addSettingsPageToAdminMenu END

    public function loadSettingsPageCSS() {
        if ( 'settings_page_mgl-tubelab' == get_current_screen()->id) {
            wp_enqueue_style("mgl_tubelab_admin", MGL_TUBELAB_URL_BASE . "/assets/css/mgl_tubelab_admin.css", false, "1.0", "all" );
        }
    }//loadSettingsPageCSS END
    
    public function tubelabSettingsPage() {  
        if( isset($_POST) ) $this->updateSettings();
        $this->mglTubelabSettings = $this->getSettings();
        
        $this->renderSettingsPage();

    }//youtubeGallerySettingsPage END

    public function updateSettings(){
        $updated = false;

        if(isset($_POST['mgl_tubelab'])) {
            $settings = array_merge( $this->getSettings(), $_POST['mgl_tubelab'] );
            //TODO: Hacer sanatize de las settings que vienen del formulario
            update_option( 'mgl_tubelab', $settings );

            $updated = true;
        }

        if($updated == true) {
            $messageSettingsSaved = __('Settings saved', MGL_TUBELAB_DOMAIN );
            $this->addFlashMessage( $messageSettingsSaved, self::STATUS_MESSAGE_UPDATED );
        }
    }//updateSettings END

    public function getSettings(){
        $dbSettings = get_option('mgl_tubelab', array(), true);
        $settings = ( is_array( $dbSettings) ) ? $dbSettings : array();
        
        $settings = array_merge( $defaults = array(
            'configuration' => array( 
                'apiKey' => '',  
                'jquery' => 1,
                ),
            'settings' => array(
                'size'          => 'medium',
                'cols'          => 4,
                'mode'          => 'lightbox',
                'template'      => 'default',
                'custom_templates'  => '',
                'count'         => 12,
                'display'       => 'title,description,meta',
                'cache'         => 3600,
                'embedRatio'    => array(
                        'width'     => 320,
                        'height'    => 180,
                    )
                )
            ), $settings );
        
        return $settings;
    }

    public function addFlashMessage( $message, $messageType ){
        $this->flashMessageBag[] = array( $messageType => $message );
    }//addFlashMessage END

    public function renderFlashMessageBag(){
        foreach( $this->flashMessageBag as $flashMessage ){
            foreach( $flashMessage as $messageType => $message ){
                ?>
                    <div class="<?php echo $messageType; ?>"> 
                        <p><strong><?php echo $message ?></strong></p>
                    </div>
                <?php
            }
        }
        $this->flashMessageBag = array();
    }

    public function renderSettingPageTabs( $current = 'youtube-app' ) {
        $tabs = array( 
            'configuration'     => __('Configuration',MGL_TUBELAB_DOMAIN), 
            'default-settings'  => __('Default settings',MGL_TUBELAB_DOMAIN), 
            'documentation'     => __('Documentation',MGL_TUBELAB_DOMAIN) 
        );

        echo '<h2 class="nav-tab-wrapper">';
        foreach( $tabs as $tab => $name ){
            $class = ( $tab == $current ) ? ' nav-tab-active' : '';
            echo "<a class='nav-tab$class' href='?page=mgl-tubelab&tab=$tab'>$name</a>";
        }
        echo '</h2>';
    }//renderSettingPageTabs END

    public function renderSettingsPage(){
        $currentTab = ( isset($_GET['tab']) ) ?  $_GET['tab'] : 'configuration';
        ?>
            <div class="wrap">
                <h1><?php _e( 'Tubelab by MaGeek Lab', MGL_TUBELAB_DOMAIN ); ?></h1>
                <?php 
                    $this->renderSettingPageTabs( $currentTab ); 
                    $this->renderFlashMessageBag(); 
                    $this->renderSettingPageCurrentTab( $currentTab ); 
                ?>
            </div>
        <?php
    }//renderSettingsPage END

    public function renderSettingPageCurrentTab( $currentTab ){
        switch ( $currentTab ) {
            case 'configuration':
                $this->renderConfigurationTab();
                break;
            case 'documentation':
                $this->renderDocumentationTab();
                break;
            case 'default-settings':
            default:
                $this->renderDefaultSettingsTab();
                break;
        }
    }//renderSettingPageCurrentTab END

    public function renderConfigurationTab(){
        include('admin-settings-tabs/configuration-tab.php');
    }//renderConfigurationTab END

    public function renderDocumentationTab(){
        include( 'admin-settings-tabs/documentation-tab.php');
    }//renderDocumentationTab END

    public function renderDefaultSettingsTab(){
        include( 'admin-settings-tabs/settings-tab.php');
    }//renderInstagramAppTab END

}
$mglTubelabAdmin = new MGL_TubelabAdmin();