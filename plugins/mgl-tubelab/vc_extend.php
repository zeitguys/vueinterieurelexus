<?php 

add_action('init', 'mgl_tubelab_map_shortcodes_in_vc');

if ( defined( 'WPB_VC_VERSION' ) ) {
  mgl_tubelab_map_shortcodes_in_vc();
}

function mgl_tubelab_map_shortcodes_in_vc() {
  if ( defined( 'WPB_VC_VERSION' ) ) {

  $default_params = array(
    array(
      "type"        => "textfield",
      "holder"      => "strong",
      "heading"     => __("Username", MGL_TUBELAB_DOMAIN),
      "param_name"  => "user",
      "description" => __("Username to pull videos from", MGL_TUBELAB_DOMAIN)
    ),
    array(
      "type"        => "textfield",
      "holder"      => "strong",
      "heading"     => __("Channel ID", MGL_TUBELAB_DOMAIN),
      "param_name"  => "channel_id",
      "description" => __("ID of the channel to pull videos from", MGL_TUBELAB_DOMAIN)
    ),
    array(
      "type"        => "textfield",
      "heading"     => __("Count", MGL_TUBELAB_DOMAIN),
      "param_name"  => "count",
      "value"       => mgl_tubelab_option('count', 12),
      "description" => __("Number of videos to display", MGL_TUBELAB_DOMAIN)
    ),
    array(
      "type"        => "dropdown",
      "heading"     => __("Columns", MGL_TUBELAB_DOMAIN),
      "param_name"  => "cols",
      "value"       => mgl_tubelab_cols(true),
      "std"         => mgl_tubelab_option('cols', 4),
      "description" => __("Number of columns of the gallery", MGL_TUBELAB_DOMAIN)
    ),
    array(
      "type"        => "textfield",
      "holder"      => "p",
      "class"       => "",
      "heading"     => __("Display", MGL_TUBELAB_DOMAIN),
      "param_name"  => "display",
      "description" => __("Parts of the video you want to display, set none to only show the video itself", MGL_TUBELAB_DOMAIN),
      "value"       => mgl_tubelab_option('display','title,description,meta'),
      "group"       => __("Style", MGL_TUBELAB_DOMAIN)
    ),
    array(
      "type"        => "dropdown",
      "heading"     => __("Mode", MGL_TUBELAB_DOMAIN),
      "param_name"  => "mode",
      "value"       => mgl_tubelab_modes(true),
      "std"         => mgl_tubelab_option('mode', 'lightbox'),
      "description" => __("Choose how the videos are played", MGL_TUBELAB_DOMAIN)
    ),
    array(
      "type"        => "dropdown",
      "heading"     => __("Template", MGL_TUBELAB_DOMAIN),
      "param_name"  => "template",
      "value"       => mgl_tubelab_templates(),
      "std"         => mgl_tubelab_option('template', 'default'),
      "description" => __("Choose the layout you most like to display your videos", MGL_TUBELAB_DOMAIN),
      "group"       => __("Style", MGL_TUBELAB_DOMAIN)
    ),
    array(
      "type"        => "dropdown",
      "heading"     => __("Thumbnail size", MGL_TUBELAB_DOMAIN),
      "param_name"  => "size",
      "value"       => mgl_tubelab_sizes(true, true),
      "description" => __("The size of the thumbnail size", MGL_TUBELAB_DOMAIN),
      "group"       => __("Style", MGL_TUBELAB_DOMAIN),
      "std"         => mgl_tubelab_option('size', 'medium')
    ),
    array(
      "type"        => "dropdown",
      "heading"     => __("Pagination", MGL_TUBELAB_DOMAIN),
      "param_name"  => "pagination",
      "value"       => array(
          __("Yes", MGL_TUBELAB_DOMAIN) => "true",
          __("No", MGL_TUBELAB_DOMAIN) => "false"
        ),
      "group"       => __("Advanced", MGL_TUBELAB_DOMAIN)
    )          
  );

  $shortcodes = array(
      'mgl_tubelab_channel' => array(
            'name'  => __("Tubelab Channel", MGL_TUBELAB_DOMAIN),
            'params' => array(
                array(
                  "type"        => "textfield",
                  "holder"      => "p",
                  "class"       => "",
                  "heading"     => __("Playlists", MGL_TUBELAB_DOMAIN),
                  "param_name"  => "playlists",
                  "description" => __("If you don't want to display all the playlists of the channel set it's ID like uploads or likes", MGL_TUBELAB_DOMAIN),
                  "value"       => 'all'
                )
            )
      ),
      'mgl_tubelab_playlist' => array(
            'name'  => __("Tubelab Videos", MGL_TUBELAB_DOMAIN),
            'params' => array(
                array(
                  "type"        => "dropdown",
                  "heading"     => __("Type", MGL_TUBELAB_DOMAIN),
                  "param_name"  => "type",
                  "value"       => mgl_tubelab_playlist_types(true),
                  "description" => __("Where do you want to display the videos from", MGL_TUBELAB_DOMAIN)
                ),
                array(
                  "type"        => "textfield",
                  "holder"      => "strong",
                  "heading"     => __("Value", MGL_TUBELAB_DOMAIN),
                  "param_name"  => "value",
                  "description" => __("The ID from the playlist, if it's from an user use uploads or likes", MGL_TUBELAB_DOMAIN)
                )
            )
      )
  );

    foreach ($shortcodes as $key => $shortcode) {

      vc_map( array(
        "name"              => $shortcode['name'],
        "description"       => __("By MaGeek Lab", MGL_TUBELAB_DOMAIN),
        "base"              => $key,
        "class"             => "mgl_tubelab",
        "controls"          => "full",
        "icon"              => "mgl_tubelab",
        "category"          => __('Social', MGL_TUBELAB_DOMAIN),
        "admin_enqueue_css" => array(plugins_url('assets/css/mgl_tubelab_admin.css', __FILE__)),
        "params"            => array_merge($shortcode['params'], $default_params)
      ) );

    }

    vc_map( array(
      "name"              => __("Tubelab Single Video", MGL_TUBELAB_DOMAIN),
      "description"       => __("By MaGeek Lab", MGL_TUBELAB_DOMAIN),
      "base"              => 'mgl_tubelab_video',
      "class"             => "mgl_tubelab",
      "controls"          => "full",
      "icon"              => "mgl_tubelab",
      "category"          => __('Social', MGL_TUBELAB_DOMAIN),
      "admin_enqueue_css" => array(plugins_url('assets/css/mgl_tubelab_admin.css', __FILE__)),
      "params"            => array(
        array(
          "type"        => "textfield",
          "holder"      => "strong",
          "heading"     => __("Video ID", MGL_TUBELAB_DOMAIN),
          "param_name"  => "video_id",
          "description" => __("The ID of the video, you can get it from the url like https://www.youtube.com/watch?v=XXXXX, where XXXXX is the ID", MGL_TUBELAB_DOMAIN)
        ),
        array(
          "type"        => "dropdown",
          "heading"     => __("Mode", MGL_TUBELAB_DOMAIN),
          "param_name"  => "mode",
          "value"       => mgl_tubelab_modes(true),
          "std"         => mgl_tubelab_option('mode', 'lightbox'),
          "description" => __("Choose how the videos are played", MGL_TUBELAB_DOMAIN)
        ),
        array(
          "type"        => "dropdown",
          "heading"     => __("Template", MGL_TUBELAB_DOMAIN),
          "param_name"  => "template",
          "value"       => mgl_tubelab_templates(),
          "std"         => mgl_tubelab_option('template', 'default'),
          "description" => __("Choose the layout you most like to display your videos", MGL_TUBELAB_DOMAIN),
          "group"       => __("Style", MGL_TUBELAB_DOMAIN)
        ),
        array(
          "type"        => "textfield",
          "holder"      => "p",
          "class"       => "",
          "heading"     => __("Display", MGL_TUBELAB_DOMAIN),
          "param_name"  => "display",
          "description" => __("Parts of the video you want to display, set none to only show the video itself", MGL_TUBELAB_DOMAIN),
          "value"       => 'title,channel,description,meta',
          "group"       => __("Style", MGL_TUBELAB_DOMAIN)
        ),
        array(
          "type"        => "dropdown",
          "heading"     => __("Thumbnail size", MGL_TUBELAB_DOMAIN),
          "param_name"  => "size",
          "value"       => mgl_tubelab_sizes(true, true),
          "std"         => mgl_tubelab_option('size', 'medium'),
          "description" => __("The size of the thumbnail size", MGL_TUBELAB_DOMAIN),
          "group"       => __("Style", MGL_TUBELAB_DOMAIN),
          "std"         => "medium"
        ),
      )
    ) );

  }
}

?>