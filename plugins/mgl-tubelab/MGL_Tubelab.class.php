<?php
class MGL_Tubelab {
    
    public function __construct(){
       /* //Add jQuery to public side of the web
        if(get_option('mgl_youtube_jquery', true) == true) {
            add_action('wp_print_scripts', array( $this, 'enqueueJQueryToPublicPartWeb' ) );
        }*/

        //Register and Styles and Scripts
        add_action('init', array( $this, 'registerStylesAndScripts' ) );

        //Shortcodes
        add_shortcode('mgl_tubelab_channel', array( $this, 'galleryShortcodes' ) );
        add_shortcode('mgl_tubelab_playlist', array( $this, 'galleryShortcodes' ) );
        add_shortcode('mgl_tubelab_video', array( $this, 'galleryShortcodes' ) );
        
        //Ajax events
        add_action( 'wp_ajax_mgl_tubelab_loadContent', array( $this, 'loadTubelabContent' ) );
        add_action( 'wp_ajax_nopriv_mgl_tubelab_loadContent', array( $this, 'loadTubelabContent' ) );
    }

    public function enqueueJQueryToPublicPartWeb() {
        if(!is_admin()) {
            wp_enqueue_script("jquery"); 
        }
    }

    public function registerStylesAndScripts(){
        // Setup needed libraries
        wp_register_style('mgl_tubelab', MGL_TUBELAB_URL_ASSETS . 'css/mgl_tubelab.css');
        wp_register_style('mgl_tubelab_magnific_popup', MGL_TUBELAB_URL_ASSETS . 'css/magnific_popup.css');

        wp_register_script('mgl_tubelab_loader', MGL_TUBELAB_URL_ASSETS . 'js/tubelab-loader.js');
        wp_register_script('mgl_tubelab_fitvids', MGL_TUBELAB_URL_ASSETS . 'js/libs/jquery.fitvids.js');
        wp_register_script('mgl_tubelab_magnific_popup', MGL_TUBELAB_URL_ASSETS . 'js/libs/jquery.magnific-popup.min.js');

        wp_localize_script('mgl_tubelab_loader', 'ajax_object',array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    }

    public function galleryShortcodes($atts, $content = null, $tagName) {

        $atts = $this->sanatizeShortcodeArguments( $atts );
        
        $atts = shortcode_atts(array(
                'type'      => '',
                'value'     => '',
                'user'      => '',
                'channel_id'=> '',
                'count'     => mgl_tubelab_option('count'),
                'cols'      => mgl_tubelab_option('cols'),
                'mode'      => mgl_tubelab_option('mode'),
                'template'  => mgl_tubelab_option('template'),
                'display'   => mgl_tubelab_option('display'),
                'pagination'=> 'true',
                'size'      => mgl_tubelab_option('size'),
                'video_id'  => '',
                'playlists' => 'all',
                'mini'      => false
        ), $atts);

        try{
            $gallery = $this->getGalleryByTagName( $tagName, $atts );
            
            return $gallery->render();
        }
        catch( Exception $e){
            _e('MGL Tubelab error: '.$e->getMessage() );
        }
    }

    private function getGalleryByTagName( $tagName, $args ){
        switch ($tagName) {
            case 'mgl_tubelab_playlist':
                $gallery = new MGL_Tubelab_PlaylistController( $args );
                break;
            
            case 'mgl_tubelab_channel':
                //$args['playlists'] = 'all';
                $gallery = new MGL_Tubelab_ChannelController( $args );
                break;

            case 'mgl_tubelab_video':
                //$args['video_id'] = '';
                $gallery = new MGL_Tubelab_VideoController( $args );
                break;

            default:
                $gallery = false;
                break;
        }

        return $gallery;
    }

    public function sanatizeShortcodeArguments( $args = array() ) {
        
        if(!empty($args)) {
            foreach( $args as $key => $value ){
                if( $value == '' ){
                    unset( $args[ $key ] );
                }else{
                    $args[ $key ] = trim( $value );
                }
            }
        }
        return $args;
    }

    public function loadTubelabContent() {

        $parametersString   = (string)$_POST['parameters'];
        $parameters         = array();
        parse_str($parametersString, $parameters);
        
        if(isset($_POST['pageToken'])) {

           $pageToken = (string)$_POST['pageToken']; 
           $parameters['pageToken'] = $pageToken;
           
        }

        $gallery = new MGL_Tubelab_PlaylistController( $parameters );
        
        $response = array(
            'content' => wp_encode_emoji($gallery->renderContent()),
            'nextId' => $gallery->nextId,
            'prevId' => $gallery->prevId
        );

        die( json_encode( $response ) );
    }
}

$mglTubelab = new MGL_Tubelab();
