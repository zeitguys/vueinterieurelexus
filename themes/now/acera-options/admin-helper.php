<?php

function acera_admin_styles()
{
    wp_enqueue_style('acera-font', 'http://fonts.googleapis.com/css?family=Rokkitt');
    wp_enqueue_style('acera-acera_css', get_template_directory_uri().'/acera-options/css/acera_css.css');
    wp_enqueue_style('acera-colorpicker', get_template_directory_uri().'/acera-options/css/colorpicker.css');
    wp_enqueue_style('acera-custom_style', get_template_directory_uri().'/acera-options/css/custom_style.css');

    wp_enqueue_script('acera-js-colorpicker', get_template_directory_uri().'/acera-options/js/colorpicker.js');
    wp_enqueue_script('acera-js-ajaxupload', get_template_directory_uri().'/acera-options/js/ajaxupload.js');
    wp_enqueue_script('acera-js-main', get_template_directory_uri().'/acera-options/js/mainJs.js');
}
