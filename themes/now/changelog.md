
# Changelog
## 2015-01-11 12:06
* Admin page optimalizations and fixes
* WP 4.* support
* New loading animations
* New Search toolbar
* Bug fixes for Android google chrome and iOS
* Add to homescreen popup option added 
* Updated javascript plugins
* Touch gestures disabled by default