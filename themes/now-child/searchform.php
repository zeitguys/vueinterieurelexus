<form role="search" method="get" id="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="search-form">
		<span id="search-form-close">&times;</span>
		<label for="search-input-s" class="hide"><?php _e('Search for', 'now'); ?></label>
		<input placeholder="<?php _e('Search for', 'now'); ?>" type="text" value="" name="s" id="search-input-s" />
		<input type="submit" class="webFont" id="searchsubmit" value="L" tabindex="-1" />
	</div>
</form>