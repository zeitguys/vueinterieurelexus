jQuery( document ).ready(function($) {
  // Search
    $('#search-input-s').focus(function() {
      $('#search-form').addClass('active');
    });

    $('#search-input-s').blur(function() {
      $('#search-form').removeClass('active');
    });

    // Move PDF button to inside 'addthis'
    if ($('.addthis_toolbox').length) {
      $('#pdf-icon').each(function() {
        $(this).parent('a')
            .addClass('at300b')
            .prependTo($('.addthis_toolbox'));
      });
    }

    // Open City
    $('#tab-view .w3-button').click(function() {
      var city = $(this).html();
      $('.w3-container').hide();
      $('.w3-container#' + city).show();
    });

    $('.mobmenu-right-bt').focus(function() {
	    $('body').addClass('show-nav-right');
    });
});
